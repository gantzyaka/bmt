﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using GameTypes;

public class SoundManager : MonoBehaviour {

    public AudioSource[] SFXSources;
    public AudioSource[] MusicSource;    

    [Serializable]
    public class SFXKeyPair
    {
        public SFXType Key;
        public AudioClip Clip;
    }

    [Serializable]
    public class MusicKeyPair
    {
        public MusicType Key;
        public AudioClip[] Clips;
    }

    [Header("Sounds")]
    public List<SFXKeyPair> SFXClips;       

    [Header("Music")]
    public List<MusicKeyPair> MusicClips;

    private Dictionary<SFXType, AudioClip> SFXClipsDic;
    private Dictionary<MusicType, AudioClip[]> MusicClipsDic;

    public static SoundManager instance { get; private set; }

    private MusicType curMusType;
    private int curMusIndex = 0;
    private float duration = 1.0f;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);

        SFXClipsDic = new Dictionary<SFXType, AudioClip>();
        foreach (var kvp in SFXClips)
            SFXClipsDic.Add(kvp.Key, kvp.Clip);

        MusicClipsDic = new Dictionary<MusicType, AudioClip[]>();
        foreach (var kvp in MusicClips)
            MusicClipsDic.Add(kvp.Key, kvp.Clips);
    }

    private void Start()
    {
        PlayMusicHandler(MusicType.MainMenu);
    }

    public void PlaySingle(AudioClip clip, bool applyDuplicates = true)
    {
        if (!applyDuplicates && SFXSources.Any(x => x.clip == clip && x.isPlaying)) return;

        AudioSource source = SFXSources.FirstOrDefault(x => !x.isPlaying);
        if (source == null)        
            source = SFXSources[0];
        
        source.clip = clip;
        source.Play();
    }    

    public void PlaySFXHandler(SFXType type)
    {
        if (!SFXClipsDic.ContainsKey(type)) return;

        PlaySingle(SFXClipsDic[type], applyDuplicates: type != SFXType.Damage && type != SFXType.Heal); //звуки дамаги и лечения не должны дублироваться, иначе происходит в ходу их жуткое наложение
    }

    public void ButtonClickSound()
    {
        PlaySFXHandler(SFXType.PressButton);
    }

    public void CardClickSound()
    {
        PlaySFXHandler(SFXType.MoveTable);
    }

    public void PlayMusicHandler(MusicType type)
    {
        if (!MusicClipsDic.ContainsKey(type) || curMusType == type) return;

        curMusType = type;
        PlayRandomizeMusic(MusicClipsDic[type]);
    }

    public void PlayRandomizeMusic(params AudioClip[] clips)
    {
        if (clips == null || clips.Length == 0) return;              

        if (MusicSource != null && MusicSource[curMusIndex].clip != null && clips.Contains(MusicSource[curMusIndex].clip))
            clips = clips.Where(x => x != MusicSource[curMusIndex].clip).ToArray();        

        int index = UnityEngine.Random.Range(0, clips.Length);
        PlayMusic(clips[index]);
    }

    public void PlayMusic(AudioClip clip)
    {
        if (clip == null) return;

        int oldMusIndex = curMusIndex;
        curMusIndex = curMusIndex == 0 ? 1 : 0;

        MusicSource[curMusIndex].clip = clip;
        MusicSource[curMusIndex].Play();
        
        StartCoroutine(FadeAudioSource(MusicSource[curMusIndex], duration, 0.5f));

        if (MusicSource[oldMusIndex].clip != null)
            StartCoroutine(FadeAudioSource(MusicSource[oldMusIndex], duration, 0.0f));
    }

    IEnumerator FadeAudioSource(AudioSource source, float duration, float targetVolume)
    {        
        int steps = (int)(15 * duration);
        float stepTime = duration / steps;
        float stepSize = (targetVolume - source.volume) / steps;
        
        for (int i = 1; i < steps; i++)
        {
            source.volume += stepSize;
            yield return new WaitForSeconds(stepTime);
        }
        
        source.volume = targetVolume;
    }

    private void FixedUpdate()
    {
        if (!MusicSource[curMusIndex].isPlaying && curMusType != MusicType.None)
            PlayRandomizeMusic(MusicClipsDic[curMusType]);
    }
}
