﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using GameTypes;


public class BuffInfo : InfoEntity
{
    public Text PointsText;
    public Image PointsBackImg;

    public void SetBuff(Buff b)
    {
        if (b == null || (!b.IsClassAbility && b.SourceCardId < 1) || (b.IsClassAbility && b.SourceCharId < 1) || b.SourceCardId > Lib.CardsDic.Count) return;

        Id = b.SourceCardId;

        if (!b.IsClassAbility)
            Img.sprite = Lib.instance.BuffsImgsDic.ContainsKey(b.SourceCardId) ? Lib.instance.BuffsImgsDic[b.SourceCardId] : Lib.instance.BuffsImgsDic[0];
        else
            Img.sprite = Lib.instance.BuffsClassAbilitiesImgsDic.ContainsKey(b.SourceCharId) ? Lib.instance.BuffsClassAbilitiesImgsDic[b.SourceCharId] : Lib.instance.BuffsClassAbilitiesImgsDic[0];


        if (!b.IsOnTurnsView)
        {
            PointsBackImg.gameObject.SetActive(true);
            PointsText.text = b.Points.ToString();
        }
        else
            PointsBackImg.gameObject.SetActive(false);
    }
}
