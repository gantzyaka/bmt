﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AnimationEvent : MonoBehaviour {
    
    public UnityEvent MyEvent;    

    public void Complete()
    {
        if (MyEvent != null) MyEvent.Invoke();        
    }

    
}
