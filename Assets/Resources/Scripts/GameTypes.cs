﻿using System.Collections.Generic;
using System.Threading;
using System;
using System.Linq;


namespace GameTypes
{
    public class Game : IDisposable
    {
        public int Round { get; private set; } //текущий раунд
        public int Turn { get; private set; } //текущий ход
        private int TurnStep; //выполнение текущего хода по шагам (с помощью таймера)
        public GameStages Stage { get; private set; }
        private List<Place[,]> Desk = new List<Place[,]>(); //места для карт на столе, каждй раунд - массив карт игроков
        private Timer Timer;
        public event DelTurnStep<StepEventArgs> OnNewTurnStep; //событие тика таймера, когда текущий ход выполняется по шагам (TurnStep)
        public event DelRoundInfo OnUpdateRoundInfo; //событие обновления состояний раунда
        private List<Effect> TurnEffects = new List<Effect>(); //эффекты текущего хода (каждый ход добавляются с разыгранных карт согласно приоритету, влияют на последущие - и под конец хода сбрасываются)        

        public Game()
        {
            Round = 0;
            Turn = 0;
            TurnStep = 0;
            Stage = GameStages.Put;
            Desk.Clear();
            TurnEffects.Clear();

            AddNewRound();
        }

        //подсчет закрытых копий (на количество найденных копий у противника уменьшается число карт в руке, которое видит мой игрок)
        public int GetCopiesCountAtCurrentRound(int player, bool untilCurrentTurn = false)
        {
            if (player < 0 || player > 1 || Desk == null || this.Round >= Desk.Count || this.Round < 0) return 0;

            int count = 0;
            int untilTurn = untilCurrentTurn ? Turn : 0;

            for (int i = 2; i >= untilTurn; i--) //итерация с конца, потому что считаем только закрытые копии у оппонента, двигаясь справа налево
            {
                if (Desk[this.Round][player, i].IsCopy)
                    count++;
            }

            return count;
        }        

        public List<CardWithWay> GetPlayedCardsAtLastRound(int player)
        {
            if (player < 0 || player > 1 || Desk == null || this.Round >= Desk.Count || this.Round < 0) return null;

            List<CardWithWay> tuples = new List<CardWithWay>();
            Place pl;

            for (int i = 0; i < 3; i++)
            {
                pl = Desk[this.Round][player, i];

                if (pl.IsCopy) continue; //копии не нужны

                Card c = new Card(pl.Card.Id);                

                tuples.Add(new CardWithWay(c, pl.SpecialWayAfterRound));
            }

            return tuples;
        }

        public Place[,] GetDeskMap(int player) { return GetDeskMap(player, this.Round); }
        public Place[,] GetDeskMap(int player, int round)
        {
            if (player < 0 || player > 1 || Desk == null || round >= Desk.Count || round < 0) return null;

            Place[,] pl = new Place[2, 5]; //2 игрока, 5 ходов

            bool mirror = player == 0 ? false : true; //каждый игрок должен видеть свои карты внизу, а врага вверху. если игрок в общем столе хранится не в положении снизу(0), то в результате нужно отразить его положение с врагом.

            //в Desk хранятся раунды отдельно (по 3 карточных хода), а надо вернуть 5 ходов с картами: по 1 карте с последнего хода предыдущего раунда, по 3 карты текущего раунда, и по 1 карте первого хода следущего раунда            
            for (int i = 0; i < 2; i++)
            {
                int m = i;
                if (mirror) m = i == 0 ? 1 : 0;

                for (int j = 0; j < 5; j++)
                {
                    if (j == 0)
                    {
                        pl[i, j] = round > 0 ? Desk[round - 1][m, 2] : null;
                        continue;
                    }

                    if (j == 4)
                    {
                        pl[i, j] = round < Desk.Count - 1 ? Desk[round + 1][m, 0] : null;
                        continue;
                    }

                    pl[i, j] = Desk[round][m, j - 1];
                }
            }

            return pl;
        }

        public bool AddEffect(Effect ef)
        {
            TurnEffects.Add(ef);
            return true;
        }

        public bool CheckForEffect(EffectTypes et)
        {
            return TurnEffects.Any(x => x.EffectType == et);
        }

        public Effect GetEffect(EffectTypes et)
        {
            return TurnEffects.FirstOrDefault(x => x.EffectType == et);
        }

        public void NextTimer()
        {
            Timer?.Dispose();
            Timer = new Timer(TimerTick, null, 2000, 2000);
        }

        private void TimerTick(object obj)
        {
            TurnStep++;
            OnNewTurnStep?.Invoke(this, new StepEventArgs(TurnStep));

            if (TurnStep >= 3) //последний шаг(реакт стадия) - убить таймер
            {
                TurnStep = 0;
                TimerKill();
            }
        }

        public bool NextTurn()
        {
            TurnEffects.Clear();
            TurnStep = 0;

            bool nextRound = false;

            if (Turn == 3)
            {
                AddNewRound();
                Round++;
                Turn = 0;
                Stage = GameStages.Put;
                nextRound = true;
            }
            else
            {
                Turn++;
                Stage = GameStages.Turn;
            }

            OnUpdateRoundInfo?.Invoke(GetRoundInfo());

            return nextRound;
        }

        public void NextReact()
        {
            Stage = GameStages.React;

            OnUpdateRoundInfo?.Invoke(GetRoundInfo());
        }

        public void AddNewRound()
        {
            Place[,] pl = new Place[2, 3]; //2 игрока, 3 места для карты у каждого на столе            

            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    pl[i, j] = new Place();
                }
            }

            Desk.Add(pl);
        }

        public Place GetPlace(int player, int placeId)
        {
            if (player < 0 || player > 1 || placeId < 0 || placeId > 2 || Round >= Desk.Count)
                throw new System.ArgumentException("Game.GetPlace() Error: player = " + player + " ; placeId = " + placeId + " ; Round = " + Round + " ; Desk.Count = " + Desk?.Count);

            return Desk[Round][player, placeId];
        }

        private string GetRoundInfo()
        {
            string s = "Раунд " + (Round + 1) + ": ";

            switch (Stage)
            {
                case GameStages.Put:
                    s += "Выкладка карт";
                    break;

                case GameStages.Turn:
                    s += "Ход : " + Turn;
                    break;

                case GameStages.React:
                    s += "Ход : " + Turn + " React - стадия";
                    break;
            }

            return s;
        }

        public void SubscribeOnEvents(DelTurnStep<StepEventArgs> delTurnStep, DelRoundInfo delRoundInfo)
        {
            if (delTurnStep == null || delRoundInfo == null) return;

            OnNewTurnStep += delTurnStep;
            OnUpdateRoundInfo += delRoundInfo;

            OnUpdateRoundInfo?.Invoke(GetRoundInfo());
        }

        public void Dispose()
        {
            Desk.Clear();
            TurnEffects.Clear();
            TimerKill();

            if (OnNewTurnStep != null)
                foreach (DelTurnStep<StepEventArgs> del in OnNewTurnStep.GetInvocationList())
                    OnNewTurnStep -= del;

            if (OnUpdateRoundInfo != null)
                foreach (DelRoundInfo del in OnUpdateRoundInfo.GetInvocationList())
                    OnUpdateRoundInfo -= del;
        }

        public void TimerKill()
        {
            if (Timer != null)
                Timer.Dispose();
        }
    }

    public class Place
    {        
        public Card Card { get; private set; }
        public Target Target { get; private set; } = Target.None;
        public bool IsCopy { get; private set; } //карта в этом месте является копией карты из места левее        
        public WayForCard SpecialWayAfterRound { get; private set; } = WayForCard.None; //во время розыгрыша карт, здесь помечается место, карту в котором под конец раунда надо отправить по нестандартному пути (в могилу или вернуть в колоду)

        public void SetCard(Card c, bool isCopy = false)
        {
            Card = isCopy ? new Card(c.Id) : c;
            IsCopy = isCopy;
            Target = Target.None; //сброс цели каждый раз, когда в месте меняется карта
        }

        public void SetTarget(int dir)
        {
            Target = dir == 0 ? Target.Enemy : Target.Me;
        }

        public void SetSpecialWayAfterRound(WayForCard type)
        {
            if (SpecialWayAfterRound != WayForCard.ToGrave) //отправку в могилу нельзя другим направлением перебивать
                SpecialWayAfterRound = type;
        }
    }



    public class Character
    {
        public readonly int Id;
        public readonly int HP;
        public readonly string Name;
        public readonly string Discription;
        public readonly bool HasLimitHP;

        public Character(int id, int hP, string name, string discription, bool hasLimitHP = true)
        {
            Id = id;
            HP = hP;
            Name = name;
            Discription = discription;
            HasLimitHP = hasLimitHP;
        }
    }

    public enum AnimTypes { FadeInImage, FadeOutImage, FadeInText, FadeOutText };

    public enum CardRarityes { Common, Ultimate };
    public enum CardTypes { Ancient = 0, C_Archmage = 1, C_Blastmage = 2, C_Necromancer = 3, C_Demonic = 4, C_Vampire = 5 };

    //!!!здесь важен порядок!!! птмч сортировка по EffectTypes в ходу
    //CancelBuffs должен быть в конце, в последнюю очередь должны быть отмена бафов (после их навешивания) 
    //CancelActions и CancelAction должны быть в начале, чтобы заранее отменять все на этом же приоритете в этом же ходу 
    //MirrorActions должен идти сразу после Cancel'ов, чтоб разворачивать все дальнейшие действия на этом же приоритете
    public enum EffectTypes
    {
        CancelActions = 0,
        CancelAction = 1,
        MirrorActions = 2,
        MirrorDamage = 3,
        Action = 4, //только вешает бафы на цели
        Heal = 5,
        Damage = 6,
        DamageUnblockable = 7,
        DropToGrave = 8,
        SwapHP = 9,
        KillIfDamageDone = 10,
        Kill = 11,
        Empty = 12, //= не выбран тип
        CancelBuffs = 13
    };

    public enum BuffTypes
    {
        NoDamage,
        NoBuffs,
        CantDie,
        AddDamage,
        IncDamage,        
        MultiplyCommonDamage,
        ShowDamageDealt,
        ActivateClassAbilities,
        HealToDamage,
        Heal,
        Damage,
        DieAtBuffEnd,
        MirrorActions,
        MirrorDamage
    };

    public enum Target { None, Me, Enemy, All, Select }
    public enum DurationTypes { EndRound, EndingCondition, TurnsLeft, EndGame } //Конец раунда, условие завершения (карта с эффектом заданного типа разыгралась), N ходов, бессрочный
    public enum ActivationTimeTypes { AtEnd, EveryTurn, Always }
    public enum CardReturnToDeckTypes { AtNextRound, Never }
    public enum WayForCard { ToGrave, ToDeck, None }


    public class Card
    {
        public readonly string Name;
        public readonly int Id; //картинки подставляются по айди карты
        public readonly string Discription;
        public readonly CardRarityes Rarity;
        public readonly CardTypes Type;
        public readonly List<Effect> Effects;
        public readonly bool IsBlockedForCopying; //не может быть скопирована
        public readonly CardReturnToDeckTypes ReturnType; //когда возвращется в колоду после розыгрыша

        public Card() { }

        public Card(int id)
        {
            Id = id;
            Card c = Lib.CardsDic[id];
            Name = c.Name;
            Discription = c.Discription;
            Rarity = c.Rarity;
            Type = c.Type;
            IsBlockedForCopying = c.IsBlockedForCopying;
            ReturnType = c.ReturnType;

            Effects = new List<Effect>();
            foreach (Effect ef in c.Effects)
            {
                Effects.Add(new Effect(ef));
            }
        }

        public Card(int id, string name, string discription, CardRarityes rarity, CardTypes type, List<Effect> effects, CardReturnToDeckTypes returnType, bool isBlockedForCopying = false)
        {
            Id = id;
            Name = name;
            Discription = discription;
            Rarity = rarity;
            Type = type;
            IsBlockedForCopying = isBlockedForCopying;
            Effects = effects;
            ReturnType = returnType;
        }        

        public bool CanBeCopied(int charId)
        {
            if (IsBlockedForCopying)
                return false;

            if (Rarity == CardRarityes.Common)
                return true;
            else
                return charId == 1; //Ultimate карты может только Archmage копировать
        }
    }

    public class Effect
    {
        public int Priority { get; private set; }
        public readonly EffectTypes EffectType;
        public readonly Target Target;
        public readonly int Points;
        public readonly List<Buff> Buffs;
        public int SourcePlayerId { get; private set; }

        public Effect() { }
        
        public Effect(Effect ef, int sourcePlayerId = -1)
        {
            Priority = ef.Priority;
            EffectType = ef.EffectType;
            Target = ef.Target;
            Points = ef.Points;
            SourcePlayerId = ef.SourcePlayerId != -1 ? ef.SourcePlayerId : sourcePlayerId;

            if (ef.Buffs != null)
            {
                Buffs = new List<Buff>();
                foreach (Buff b in ef.Buffs)
                {
                    Buffs.Add(new Buff(b));
                }
            }
        }

        public Effect(int priority, EffectTypes effectType, Target target, int points, List<Buff> buffs)
        {
            Priority = priority;
            EffectType = effectType;
            Target = target;
            Points = points;
            Buffs = buffs;
            SourcePlayerId = -1;
        }

        public Effect(Buff b)
        {
            Priority = b.Priority;
            EffectType = GetEffectType(b.BuffType);
            Target = b.Target;
            Points = b.Points;
            Buffs = null;
            SourcePlayerId = b.SourcePlayerId;
        }

        public static EffectTypes GetEffectType(BuffTypes bt)
        {
            EffectTypes et = EffectTypes.Empty;

            switch (bt)
            {
                case BuffTypes.Damage:
                case BuffTypes.AddDamage:
                    et = EffectTypes.Damage;
                    break;

                case BuffTypes.Heal:
                    et = EffectTypes.Heal;
                    break;

                case BuffTypes.DieAtBuffEnd:
                    et = EffectTypes.Kill;
                    break;
            }

            return et;
        }

        public void SetPriority(int pr)
        {
            if (pr < 0 || pr > 9) return;

            Priority = pr;
        }
    }

    public class Buff
    {
        public readonly int SourceCardId;
        public readonly BuffTypes BuffType;
        public readonly int Priority;
        public readonly bool IsContinuous;
        public readonly ActivationTimeTypes ActivationTime;
        public readonly bool CanStack; //баф стакается с себе подобными
        public readonly string DurationInfo;
        public readonly bool IsOnTurnsView; //активный бафф отображается: true - в ходах на столе, false - возле портрета персонажа        
        public readonly bool IsClassAbility; //баф является классовой способностью
        public Duration Duration { get; private set; }
        public int Points { get; private set; }        
        public Target Target { get; private set; } //задается при навешивании бафа, если заранее не задано (из эффекта с карты или места карты, где игрок сам выбрал направление)
        public int SourcePlayerId { get; private set; } //кто этот баф навешал, задается при навешивании //нужно, чтоб "дамаг нанесенный оппоненту" плюсовать навешавшему, если этот баф сбудет дамажить
        public int SourceCharId { get; private set; } //персонаж игрока, ктр навешал этот баф //нужно, чтоб при срабатывании дамажащего бафа, навешанного Бластмагом, дамагу присваивался приоритет 2
        public bool IsActive { get; private set; } //некоторые бафы активируются сразу, некоторые только на след. ход (когда баф активируется, это поле меняется)
        public bool IsActiveInitial { get; private set; } //поле хранит изначальное значение активации бафа при его создании - нужно для позиционирований бафов на картах на столе)


        public Buff(Buff b, int sourcePlayerId = -1, int sourceCharId = -1, Target tg = Target.None)
        {
            SourceCardId = b.SourceCardId;
            BuffType = b.BuffType;
            Duration = new Duration(b.Duration);
            Points = b.Points;
            Priority = b.Priority;
            IsContinuous = b.IsContinuous;
            ActivationTime = b.ActivationTime;
            IsActive = b.IsActive;
            IsActiveInitial = b.IsActiveInitial;
            CanStack = b.CanStack;
            IsOnTurnsView = b.IsOnTurnsView;
            IsClassAbility = b.IsClassAbility;
            SourcePlayerId = sourcePlayerId > -1 ? sourcePlayerId : b.SourcePlayerId;
            SourceCharId = sourceCharId > -1 ? sourceCharId : b.SourceCharId;

            Target = b.Target != Target.None ? b.Target : tg;
        }

        public Buff(int sourceCardId, BuffTypes buffType, Duration duration, ActivationTimeTypes activationTime = ActivationTimeTypes.Always, bool isContinuous = false, int points = 0, int priority = 0, bool isActive = true, Target target = Target.None, bool canStack = true, bool isOnTurnsView = true, bool isClassAbility = false)
        {
            SourceCardId = sourceCardId;
            BuffType = buffType;
            Duration = duration;
            Points = points;
            Priority = priority;
            IsContinuous = isContinuous;
            ActivationTime = activationTime;
            IsActive = isActive;
            IsActiveInitial = isActive;
            Target = target;
            CanStack = canStack;
            IsOnTurnsView = isOnTurnsView;
            IsClassAbility = isClassAbility;
            SourcePlayerId = -1;
            SourceCharId = -1;

            DurationInfo = GetDurationInfo();
        }

        private string GetDurationInfo()
        {
            string duration = " (" + Lib.DurationNameDic[Duration.DurType];
            if (Duration.DurType == DurationTypes.TurnsLeft) duration += Duration.Turns;
            else if (Duration.DurType == DurationTypes.EndingCondition) duration += Lib.EffectNameDic[Duration.EndingCondition];
            duration += ")";

            return duration;
        }

        public void AddPoints(int points)
        {
            Points += points;
        }

        public void SetPoints(int points)
        {
            Points = points;
        }

        public bool CheckForDie(DurationTypes dur, bool conditionIsDone = false)
        {
            bool die = false;

            if (IsActive)
            {
                switch (Duration.DurType)
                {
                    case DurationTypes.TurnsLeft:
                        die = --Duration.Turns <= 0;
                        break;

                    case DurationTypes.EndRound:
                        die = dur == DurationTypes.EndRound;
                        break;

                    case DurationTypes.EndingCondition:
                        die = dur == DurationTypes.EndingCondition && conditionIsDone;
                        break;
                }
            }
            else
                IsActive = true;

            return die;
        }        

        //для сравнения цели в эффекте, от которого должен сыграть этот баф, если задано условие с целью 
        //(здесь обыграна ситуация, когда игрок картой дамажит ВСЕХ, а в условии активации бафа задан дамаг по ВРАГУ - в этом случае баф должен активироваться)
        public bool EqualsOrIncludedInEndingConditionTarget(Target tg)
        {
            if (Duration.EndingConditionTarget == tg || (tg == Target.All && (Duration.EndingConditionTarget == Target.Me || Duration.EndingConditionTarget == Target.Enemy)))
                return true;
            else
                return false;
        }

        public int GetActivationOffset(GameStages stage)
        {
            bool isActive = stage == GameStages.Turn ? this.IsActive : this.IsActiveInitial;
            return isActive ? 0 : 1;
        }
    }

    public class Duration
    {
        public DurationTypes DurType;
        public int Turns;
        public EffectTypes EndingCondition = EffectTypes.Empty;
        public Target EndingConditionTarget = Target.None;

        public Duration() { }

        public Duration(Duration dur)
        {
            DurType = dur.DurType;
            Turns = dur.Turns;
            EndingCondition = dur.EndingCondition;
            EndingConditionTarget = dur.EndingConditionTarget;
        }
    }

    public enum CardInfoClicks { RemoveFromHand, ClickHandCard, ClickDeskCard, RemoveFromDeck, ClickDeckCard }
    public enum GameStates { Off, SelectCards, Game, SelectChar }
    public enum Actions { MyDeck, StartGame, PutCardToDesk, SetCardTarget, CopyCard, ImReady, NextTurn, NextTurnStep, SetGatherCards, MyNewCharacter }
    public enum GameStages { Put, Turn, React }
    public enum BuffsViewType { OnTurns, OnPortrait }
    public enum EndGameType { Win = 0, Lose = 1, Draw = 2, None }
    public enum SFXType { CardPlacement, CardReveal, CardTarget, Damage, Heal, MoveTable, PressButton, React, Ready, ReadyOther }
    public enum MusicType { Defeat, Draw, MainMenu, Match, Victory, None }

    //класс аргумента с шагом хода для событий таймера
    public class StepEventArgs : EventArgs
    {
        public int Step { get; set; }

        public StepEventArgs(int step)
        {
            Step = step;
        }
    }

    public delegate void DelBool(bool b);

    //делегат с аргументом - шагом хода для событий таймера
    public delegate void DelTurnStep<StepEventArgs>(object sender, StepEventArgs e);

    //делегат для метода показа/скрытия всплывающей карты-подсказки в ToolTipCardControl
    public delegate void DelToolTip(int cid, bool show);

    //делагат для обновления текстов возле иконок карт (колода, сброс, могила) у игроков
    public delegate void DelDeckIcoText(bool isEnemy, int Hand, int DeckCount, int DropCount, int GraveCount);

    //делагат для обновления иконки замены карты у игроков
    public delegate void DelReactIco(bool isEnemy, bool show);

    //делагат для обновления иконки с количеством доступных копий у игроков
    public delegate void DelCopiesIco(bool isEnemy, int count);

    //делагат для обновления иконок бафов у игроков
    public delegate void DelBuffsView(bool isEnemy, List<Buff> buffs);

    //делагат для обновления сообщения раунда
    public delegate void DelRoundInfo(string s);

    //делегат для проигрывания звука
    public delegate void DelPlaySFX(SFXType type);

    //делегат для проигрывания музыки
    public delegate void DelPlayMusic(MusicType type);

    //Разыгранный эффект с карты - для обсчитывания ходов в GameManager.CalculateTurn()
    public class PlayedCardEffect
    {
        public int PlayerId;
        public Target Target;
        public int CardId;
        public Effect Effect;
        public Place Place;
        public string Name; //имя карты или эффекта (если пассивка)
        public bool IsDisabled { get; private set; } = false;

        public PlayedCardEffect(int playerId, Target target, int cardId, Effect effect, Place place, int charId, string name)
        {
            PlayerId = playerId;
            Target = target;
            CardId = cardId; //когда эффект создается из пассивки персонажа - здесь: -1
            Name = name;
            Effect = new Effect(effect, playerId);
            Place = place; //когда эффект создаётся из бафа - здесь: null

            //у BlastMage все эффекты с карт, наносящие урон, имеют приоритет 2
            if (charId == 2 && (effect.EffectType == EffectTypes.Damage || effect.EffectType == EffectTypes.DamageUnblockable))
                Effect.SetPriority(2);
        }

        public void DisableEffect() //отменить эффект
        {
            IsDisabled = true;
        }
    }

    //класс с информацией о последней замене карты игрком в React-стадии
    public class ReactCommit
    {
        public readonly int CardId;
        public readonly int IndexFrom;
        public readonly int AtRound;
        public bool IsDone { get; private set; }

        public ReactCommit(int cardId, int indexFrom, int atRound)
        {
            CardId = cardId;
            IndexFrom = indexFrom;
            AtRound = atRound;
            IsDone = false;
        }

        public void SetIsDone()
        {
            IsDone = true;
        }
    }

    public enum DeckTypes { Hand, Deck }

    //компаратор для сравнения карт (нужен, когда: 1. начальную колоду строю, сравнивая карты в колоде-образце с уже набранными игроком картами в руку 2. отправляю разыгранные карты в сброс/могилу/колоду, так, чтоб не было дубликатов)
    public class CardsEqualityComparer : IEqualityComparer<Card>
    {
        public bool Equals(Card c1, Card c2)
        {
            if (c1 == null && c2 == null)
                return true;
            else if (c1 == null || c2 == null)
                return false;
            else if (c1.Id == c2.Id)
                return true;
            else
                return false;
        }

        public int GetHashCode(Card c)
        {
            return c.Id.GetHashCode();
        }
    }

    public class CardWithWay //а можно было бы вместо этого использовать кортеж, но Unity еще не умеет в C# 7.0
    {
        public Card Card;
        public WayForCard Way;

        public CardWithWay(Card card, WayForCard way)
        {
            Card = card;
            Way = way;
        }
    }


}
