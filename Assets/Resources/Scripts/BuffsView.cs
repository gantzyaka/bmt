﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameTypes;
using System.Linq;


public class BuffsView : MonoBehaviour {
    
    private List<BuffInfo> BuffsInfo = new List<BuffInfo>(); 
    private Transform Trans;
    public bool IsEnemy;
    public BuffsViewType Type;

    private void Awake()
    {
        Trans = transform;
    }

    public void UpdateBuffs(List<Buff> buffs)
    {
        if (buffs == null) return;

        bool isChanged = BuffsInfo.Count != buffs.Count;

        BuffInfo bi;

        //обновить иконки и, если не хаватает, - добавить
        for (int i = 0; i < buffs.Count; i++)
        {
            if (i < BuffsInfo.Count)
                bi = BuffsInfo[i];            
            else
            {
                GameObject go = Instantiate(MenuManager.instance.BuffPrefab, Vector3.zero, Quaternion.identity, Trans);
                bi = go.GetComponent<BuffInfo>();
                BuffsInfo.Add(bi);
            }                

            bi.SetBuff(buffs[i]);
        }

        //если есть лишнее - убрать
        if (BuffsInfo.Count > buffs.Count)
        {
            for (int i = BuffsInfo.Count - 1; i >= buffs.Count; i--)
            {
                bi = BuffsInfo[i];
                BuffsInfo.RemoveAt(i);
                Destroy(bi.gameObject);                
            }
        }        

        if (isChanged)
            UpdatePositions();
    }    

    public void UpdatePositions()
    {
        int count = BuffsInfo.Count;
        if (count == 0) return;

        Vector2 newPos = Vector2.zero;
        int multiplier = IsEnemy ? 1 : -1;

        Vector2[][] array;
        float scale;

        if (Type == BuffsViewType.OnPortrait)
        {
            array = Lib.BuffsPosArrayOnPortrait;
            scale = 1f;
        }
        else
        {
            array = Lib.BuffsPosArrayOnTurns;
            scale = 0.6f;
        }

        for (int i = 0; i < count; i++)
        {
            if (i > array.Length) //когда переполнение - в позицию последнего бафа все лишние позиционируются
                i = array.Length - 1;

            newPos = array[count][i];
            BuffsInfo[i].RTrans.localPosition = new Vector3 (newPos.x, newPos.y * multiplier);
            BuffsInfo[i].RTrans.localScale = new Vector3(scale, scale);            
        }
    }
}
