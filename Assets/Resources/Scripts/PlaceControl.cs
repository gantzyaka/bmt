﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameTypes;
using System.Linq;
using UnityEngine.UI;

public class PlaceControl : MonoBehaviour {

    public Button[] ArrowBtns; //0 - ArrowUp, 1 - ArrowDown
    public bool IsReady = true; //задано направление (если должно задаваться у карты)

    public Button TokenBtn;
    public Text TokenText;

    public GameObject CopyImageObj;
    public bool IsCopy;
    public Animation Anim;

    public BuffsView Buffs;
    

    public void ShowArrows()
    {
        IsReady = false;
        ArrowBtns[0].gameObject.SetActive(true);
        ArrowBtns[1].gameObject.SetActive(true);        
        StartBlinking();
    }

    public void HideArrows()
    {
        IsReady = true;
        StopBlinking();
        ArrowBtns[0].gameObject.SetActive(false);
        ArrowBtns[1].gameObject.SetActive(false);        
    }

    IEnumerator Blink()
    {
        while (true)
        {
            float alpha = ArrowBtns[0].image.color.a < 0.8f ? 1f : 0.5f;

            ArrowBtns[0].image.color = new Color(0.8f, 0.8f, 0.8f, alpha);
            ArrowBtns[1].image.color = new Color(0.8f, 0.8f, 0.8f, alpha);

            yield return new WaitForSeconds(0.5f);            
        }
    }

    public void StartBlinking()
    {
        ChangeButtonColors(ArrowBtns[1], true);
        ChangeButtonColors(ArrowBtns[0], true);

        StopAllCoroutines();
        StartCoroutine("Blink");
    }

    public void StopBlinking()
    {        
        StopAllCoroutines();
    }

    public void SetOnArrowClick(int placeId)
    {
        //Up
        ArrowBtns[0].onClick.RemoveAllListeners();
        ArrowBtns[0].onClick.AddListener(() => GameManager.instance.ClickArrow(placeId, 0));

        //Down
        ArrowBtns[1].onClick.RemoveAllListeners();
        ArrowBtns[1].onClick.AddListener(() => GameManager.instance.ClickArrow(placeId, 1));
    }

    public void SetOnTokenClick(int placeId)
    {
        TokenBtn.onClick.RemoveAllListeners();
        TokenBtn.onClick.AddListener(() => GameManager.instance.ClickToken(placeId));
    }

    public void SetTarget(Target target)
    {
        StopBlinking();        

        //вкл стрелки, их минуя мигание иногда надо показать
        ArrowBtns[0].gameObject.SetActive(true); 
        ArrowBtns[1].gameObject.SetActive(true);

        if (target == Target.Me)
        {
            ChangeButtonColors(ArrowBtns[1], true);
            ChangeButtonColors(ArrowBtns[0], false);
            IsReady = true;
        }
        else if (target == Target.Enemy)
        {
            ChangeButtonColors(ArrowBtns[1], false);
            ChangeButtonColors(ArrowBtns[0], true);
            IsReady = true;
        }
        else //не задано направление (бывает, когда переключает раунды)
        {
            IsReady = false;
            StartBlinking();
        }
    }

    private void ChangeButtonColors(Button btn, bool active)
    {
        if (btn == null) return;

        float alpha = active ? 1f : 0.5f;

        ColorBlock colors = btn.colors;
        colors.normalColor = new Color(0.8f, 0.8f, 0.8f, alpha);
        colors.highlightedColor = new Color(0.8f, 0.8f, 0.8f, alpha);
        btn.colors = colors;

        btn.image.color = colors.normalColor;
    }

    public void UpdateCopy(bool ok)
    {
        IsCopy = ok;
        if (CopyImageObj != null) CopyImageObj.SetActive(ok);        
    }

    public void PlayAnimCardOpen()
    {
        if (Anim != null) Anim.Play("ScaleIn");
    }
}
