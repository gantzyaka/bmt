﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GameTypes;

public class GUIAnimationsControl : MonoBehaviour {

    private Image[] AllImages;
    private Text[] AllTexts;
    private List<Animation> anims = new List<Animation>();    

    public bool DisableObjectWhenHide;

    public void Awake()
    {
        AllImages = GetComponentsInChildren<Image>();
        AllTexts = GetComponentsInChildren<Text>();        
    }

    public void Setup()
    {
        if (anims.Count == 0)
        {
            AnimationClip clip;
            Animation an;

            foreach (Image img in AllImages)
            {
                an = img.gameObject.GetComponent<Animation>();

                if (an == null)
                    an = img.gameObject.AddComponent<Animation>();

                clip = Lib.instance.AnimDic[AnimTypes.FadeInImage]; an.AddClip(clip, clip.name);
                clip = Lib.instance.AnimDic[AnimTypes.FadeOutImage]; an.AddClip(clip, clip.name);
                anims.Add(an);                
            }

            foreach (Text txt in AllTexts)
            {
                an = txt.gameObject.GetComponent<Animation>();

                if (an == null)
                    an = txt.gameObject.AddComponent<Animation>();

                clip = Lib.instance.AnimDic[AnimTypes.FadeInText]; an.AddClip(clip, clip.name);
                clip = Lib.instance.AnimDic[AnimTypes.FadeOutText]; an.AddClip(clip, clip.name);
                anims.Add(an);
            }
        }
    }

    public void ForcedSetup()
    {
        AllImages = GetComponentsInChildren<Image>();
        AllTexts = GetComponentsInChildren<Text>();
        anims.Clear();

        Setup();
    }

    public void Show()
    {
        Setup(); //настроить, если не настроено

        //выкрутить альфу в ноль перед показом
        Color c;
        foreach (Image img in AllImages)
            { c = img.color; c.a = 0; img.color = c; }

        foreach (Text txt in AllTexts)
            { c = txt.color; c.a = 0; txt.color = c; }        
        
        foreach (Animation an in anims)
            if (an != null)
                an.Play("FadeIn"); //анимация показа
    }

    public void Hide()
    {
        Setup(); //настроить, если не настроено
        
        foreach (Animation an in anims)        
            if (an != null)
                an.Play("FadeOut"); //анимация скрытия

        if (DisableObjectWhenHide)
            Invoke("DisableObject", 0.5f);        
    }

    private void DisableObject()
    {        
        this.gameObject.SetActive(false);
    }
}
