﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameTypes;
using System.Linq;

public class Deck : MonoBehaviour {

    private List<CardInfo> Cards = new List<CardInfo>(); //сколько всего карт в колоде
    private Transform Trans;

    public DeckTypes Type;

    public event DelBool DeckEvent;    


    private void Awake()
    {
        Trans = transform;
    }

    public void AddCardToHand(CardInfo ci) //добавление в руку карты в меню первого набора карт перед игрой (используется только для руки)
    {
        if (Type != DeckTypes.Hand || ci == null || GameManager.State != GameStates.SelectCards || Cards.Count >= 6) return;

        GameObject go = Instantiate(MenuManager.instance.CardButtonPrefab, Vector3.zero, Quaternion.identity, Trans);
        CardInfo ciNew = go.GetComponent<CardInfo>();
        ciNew.CopyCard(ci);
        Cards.Add(ciNew);
        UpdateCardsPositions();        
        
        ciNew.SetOnClick(CardInfoClicks.RemoveFromHand); //новое действие по клику на добавленной в руку карте
        ci.Btn.interactable = false; //disable CardButtonAdd        
        DeckEvent?.Invoke(DeckEventCondition());
    }

    public void AddCard(int cardId, bool updateAllCards = true) //добавление в колоду(веер) карты во время игры
    {
        if (cardId < 1 || cardId > Lib.CardsDic.Count) return;

        GameObject go = Instantiate(MenuManager.instance.CardButtonPrefab, Vector3.zero, Quaternion.identity, Trans);
        CardInfo ciNew = go.GetComponent<CardInfo>();
        ciNew.SetCard(cardId);
        Cards.Add(ciNew);

        if (updateAllCards)
        {
            UpdateCardsPositions();
            UpdateCardsOnClickToUseCard();
        }
    }

    public void AddCards(List<Card> cards)
    {
        if (cards == null || cards.Count == 0) return;

        foreach (Card c in cards)        
            AddCard(c.Id, updateAllCards: false);

        UpdateCardsPositions();
        UpdateCardsOnClickToUseCard();
    }

    private bool DeckEventCondition()
    {
        return (Type == DeckTypes.Hand && Cards.Count == 6);
    }


    public void RemoveCard(CardInfo ci)
    {
        if (ci == null) return;

        MenuManager.instance.EnableSelectCardButton(ci);
        Cards.Remove(ci);
        Destroy(ci.gameObject);
        UpdateCardsPositions();

        DeckEvent?.Invoke(DeckEventCondition());
    }

    public void RemoveCard(int index)
    {
        if (index < 0 || index >= Cards.Count) return;

        CardInfo ci = Cards[index];
        Cards.Remove(ci);
        Destroy(ci.gameObject);
        UpdateCardsPositions();
        UpdateCardsOnClickToUseCard();
    }

    public void ChangeCard(int index, int cardId)
    {
        if (index < 0 || index >= Cards.Count || cardId < 1 || cardId > Lib.CardsDic.Count) return;

        Cards[index].SetCard(cardId);
        DeselectCard(index);
    }

    

    public void ClearDeck()
    {
        foreach (CardInfo ci in Cards.ToList())
        {
            Cards.Remove(ci);
            Destroy(ci.gameObject);
        }
    }


    public void UpdateCardsPositions()
    {
        int count = Cards.Count;
        if (count == 0) return;

        //позиции карт
        int step = 80;        
        if (count > 5) step -= (count - 6) * 5; //если больше 5 карт, то сокращать расстояние между ними на 5 за каждую добавленную карту
        int offset = step * (count - 1) / 2;

        //вращение карт
        float rStep = count > 1 ? 30f / (count - 1f) : 0;
        float rOffset = count > 1 ? 30f / 2f : 0;
        if (count == 2) { rStep = 10; rOffset = 5; } //исключение для двух карт

        for (int i = 0; i < count; i++)
        {
            float x = (float)i * step - offset;
            float y = Lib.CardsYPosArray[count][i]; //тут больше 25 карт в руку нельзя пока добавлять
            Cards[i].RTrans.localPosition = new Vector3(x, y, 0);

            float zAngle = rOffset - i * rStep;
            Cards[i].RTrans.localRotation = Quaternion.Euler(0, 0, zAngle);
        }
    }



    public bool CheckForDeckReady
    {
        get { return DeckEventCondition(); }
    }

    public List<CardInfo> GetCards()
    {
        return Cards;
    }

    public void UpdateCardsOnClickToUseCard()
    {
        CardInfoClicks action = Type == DeckTypes.Hand ? CardInfoClicks.ClickHandCard : CardInfoClicks.ClickDeckCard;

        for (int i = 0; i < Cards.Count; i++)        
            Cards[i].SetOnClick(action, i);        
    }    

    public void SelectCard(int index)
    {
        MoveYCard(50, index);
    }

    public void DeselectCard(int index)
    {
        MoveYCard(-50, index);
    }

    private void MoveYCard(int offset, int index)
    {
        CardInfo ci = Cards[index];
        Vector3 pos = ci.RTrans.localPosition;
        float y = pos.y + offset;
        ci.RTrans.localPosition = new Vector3(pos.x, y, 0);
    }
}
