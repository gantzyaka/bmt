﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using GameTypes;

[RequireComponent(typeof(EventTrigger))]
public class InfoEntity : MonoBehaviour {

    public int Id;
    public Image Img;
    public RectTransform RTrans;

    private DelToolTip ToolTipUpdate;

    private void Awake()
    {
        RTrans = GetComponent<RectTransform>();
    }

    private void Start()
    {
        ToolTipUpdate = MenuManager.instance.ToolTipCardControl.UpdateToolTip;

        EventTrigger e = GetComponent<EventTrigger>();

        EventTrigger.Entry entryA = new EventTrigger.Entry();
        entryA.eventID = EventTriggerType.PointerEnter;
        entryA.callback.AddListener((data) => { ToolTipUpdate(Id, true); });
        e.triggers.Add(entryA);

        EventTrigger.Entry entryB = new EventTrigger.Entry();
        entryB.eventID = EventTriggerType.PointerExit;
        entryB.callback.AddListener((data) => { ToolTipUpdate(Id, false); });
        e.triggers.Add(entryB);
    }

    public void HideToolTip()
    {
        ToolTipUpdate(Id, false);
    }

    public void UpdateToolTip()
    {
        ToolTipUpdate(Id, true);
    }    
}
