﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using GameTypes;
using System.Linq;

public class MenuManager : MonoBehaviour {
        
    public static MenuManager instance { get; private set; }    

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);


        SelectingCards[0] = new List<CardInfo>();
        SelectingCards[1] = new List<CardInfo>();

        for (int i = 0; i < ScrollViewClassCards.childCount; i++)        
            SelectingCards[0].Add(ScrollViewClassCards.GetChild(i).GetComponent<CardInfo>());        
        
        for (int i = 0; i < ScrollViewAncientCards.childCount; i++)        
            SelectingCards[1].Add(ScrollViewAncientCards.GetChild(i).GetComponent<CardInfo>());        
    }

    public GameObject Debug;
    public Text DebugText0;
    public Text DebugText1;

    public GameObject Panel_MenuConnect;
    public InputField InputFieldIP;
    public Button ButtonStartHost;
    public Button ButtonJoinGame;
    public InputField InputName;
    public Image PortraitImage;

    public GameObject Panel_Game;
    public Button ButtonDisconnect;
    public Image[] CharsImages;
    public Image[] CharsShadowsImages;
    public Text[] PlayersNames;
    public Text[] PlayersChars;
    public Image PanelGameImage;
    public GameObject VSobj;    
    public GUIAnimationsControl CharsMiniAnimControl;
    public GUIAnimationsControl CardsSelectPanelControl;
    public GUIAnimationsControl DeckAnimControl;
    public GUIAnimationsControl DeskAnimControl;
    public GUIAnimationsControl ToolTipCardAnimControl;
    public GUIAnimationsControl ReadyTextAnimControl;
    public GUIAnimationsControl SelectCharPanelAnimControl;

    public Text[] HPs;
    public Animation[] HPsAnim;
    public Text[] HPs_Damage;
    public Animation[] HPs_DamageAnim;
    public Text[] HPs_Heal;
    public Animation[] HPs_HealAnim;


    public GameObject Panel_SelectChar;
    public Image SelectedCharImg;
    public Text SelectedCharDiscription;

    public GameObject Panel_PanelWaiting;
    public Text IpText;
    public Text WaitingConnectionText;

    public GameObject CardButtonPrefab;
    public GameObject BuffPrefab;

    public Transform ScrollViewClassCards;
    public Transform ScrollViewAncientCards;
    private List<CardInfo>[] SelectingCards = new List<CardInfo>[2];    

    public ToolTipCardControl ToolTipCardControl;
    public Deck HandControl;
    public Deck DeckControl;
    public DeskControl DeskControl;
    public Button ReadyBtn; //кнопка Готов в игре, когда выложил карты на стол
    public GUIAnimationsControl ReadyBtnAnimControl;

    public Button SelectingBtnNext; //кнопка Далее, когда набирает себе карты в руки    
    public DeskShowHideBtn DeskShowHideBtnControl; //скрипт, управляющий показом/скрытием кнопки в стадии выбора карт для добора себе в руку

    public Animation RoundInfoTextAnimation;
    public Text RoundInfo;

    public Animation TurnInfoImgAnim;
    public Animation TurnInfoTextAnim;
    public Text TurnInfo;
    
    public Animation[] PlayersReactIconAnim;
    public GameObject[] PlayersCopyIconObj;
    public Text[] PlayersCopysText;    
    public BuffsView[] BuffsViewsChars;

    [System.Serializable]
    public class DecksIcoTextsArray
    {
        public Text[] Texts;
    }
    public DecksIcoTextsArray[] DecksIcoTexts; //тексты у иконок колоды/сброса/могилы игроков
    
    public Text[] BestOfTexts; //кол-во выигрышей

    public GUIAnimationsControl VictoryLoseAnimControl;
    public Image EndGameImg;




    private void Start()
    {
        Panel_MenuConnect.SetActive(true);
        Panel_Game.SetActive(false);
        Panel_SelectChar.SetActive(false);
        Panel_PanelWaiting.SetActive(false);
        DeckControl.gameObject.SetActive(false);
        CardsSelectPanelControl.gameObject.SetActive(false);        
        ClearHPChangesText();
        ClearReactAndCopiesIcons();        
        ClearDeckTexts();
        VictoryLoseAnimControl.gameObject.SetActive(false);

        HandControl.DeckEvent += this.SelectingBtnNextEnable; //когда руку наполнит на старте матча - показывать кнопку "дальше"
        DeskControl.DeskFullEvent += this.ReadyBtnEnable; //когда карты выложит на стол- показывать кнопку "готов"
    }

    public void SwitchMenu()
    {
        if (Panel_MenuConnect.activeSelf)
        {
            Panel_PanelWaiting.SetActive(true);
            if (GameManager.isHost)
            {                
                IpText.text = CustomNetworkManager.GetMyIPAddress();
                WaitingConnectionText.text = "Это ваш внешний IP. Передайте его другу, чтобы начать с ним дуэль.\n\nУбедитесь, что порт 7777 в маршрутизаторе открыт, и игру не блокирует фаервол.";
            }
            else
            {                
                IpText.text = "Соединение с " + MenuManager.instance.InputFieldIP.text + " ...";
                WaitingConnectionText.text = "Убедитесь, что порт 7777 в маршрутизаторе открыт, и игру не блокирует фаервол.";
            }

            Panel_Game.SetActive(false);
            Panel_MenuConnect.SetActive(false);
            Panel_SelectChar.SetActive(false);            
        }        
        else if (Panel_PanelWaiting.activeSelf)
        {
            Panel_MenuConnect.SetActive(true);
            Panel_SelectChar.SetActive(false);
            Panel_Game.SetActive(false);
            Panel_PanelWaiting.SetActive(false);
        }
    }

    public void GoToMainMenu()
    {
        if (!Panel_MenuConnect.activeSelf)
        {
            Panel_MenuConnect.SetActive(true);
            Panel_SelectChar.SetActive(false);
            Panel_Game.SetActive(false);
            Panel_PanelWaiting.SetActive(false);
            CharsMiniAnimControl.gameObject.SetActive(false);
            CardsSelectPanelControl.gameObject.SetActive(false);
            DeskAnimControl.gameObject.SetActive(false);
            ToolTipCardAnimControl.gameObject.SetActive(false);
            ReadyTextAnimControl.gameObject.SetActive(false);
            ReadyBtnAnimControl.gameObject.SetActive(false);
        }
    }


    public void MenuSelectChar()
    {
        if (Panel_MenuConnect.activeSelf)
        {
            Panel_SelectChar.SetActive(true);
            SelectCharPanelAnimControl.Show();

            Panel_Game.SetActive(false);
            Panel_MenuConnect.SetActive(false);            
            Panel_PanelWaiting.SetActive(false);
        }
    }

    public void SelectChar(int a)
    {
        if (GameManager.MyCharacterId != a)
        {
            GameManager.SetMyCharacter(a);

            SelectedCharImg.sprite = Lib.instance.CharactersPortraitImages[a - 1];
            PortraitImage.sprite = Lib.instance.CharactersPortraitImages[a - 1];
            SelectedCharDiscription.text = Lib.CharsDic[a].Discription;

            PlayerPrefs.SetInt("MyCharacterId", a);
        }
    }

    public void UpdateSelectedChar(int a)
    {
        SelectedCharImg.sprite = Lib.instance.CharactersPortraitImages[a - 1];
        PortraitImage.sprite = Lib.instance.CharactersPortraitImages[a - 1];
        SelectedCharDiscription.text = Lib.CharsDic[a].Discription;
    }

    public void MenuConnect()
    {
        if (Panel_SelectChar.activeSelf)
        {
            Panel_MenuConnect.SetActive(true);
            Panel_Game.SetActive(false);            
            Panel_SelectChar.SetActive(false);
            Panel_PanelWaiting.SetActive(false);
        }
    }

    public void SaveMyName()
    {
        //Debug.Log("Name saved!");
        GameManager.SetMyName(MenuManager.instance.InputName.text);
        PlayerPrefs.SetString("MyName", MenuManager.instance.InputName.text);
    }


    public void PreGameMenuWait()
    {
        Color c = PanelGameImage.color; c.a = 0;
        PanelGameImage.color = c;

        for (int i = 0; i < 2; i++)
        {
            //скрыть картинки, тексты перед показом (тут ожидание ответа от второго игрока, чтоб данные его показать)
            CharsImages[i].gameObject.SetActive(false);
            CharsImages[i].transform.parent.gameObject.SetActive(false);
            CharsShadowsImages[i].gameObject.SetActive(false);
            PlayersNames[i].gameObject.SetActive(false);
            PlayersChars[i].gameObject.SetActive(false);
        }
        VSobj.SetActive(false);

        Panel_Game.SetActive(true);
        Panel_MenuConnect.SetActive(false);
        Panel_SelectChar.SetActive(false);
        Panel_PanelWaiting.SetActive(false);
        CharsMiniAnimControl.gameObject.SetActive(false);
        CardsSelectPanelControl.gameObject.SetActive(false);
        DeskAnimControl.gameObject.SetActive(false);
        ReadyBtnAnimControl.gameObject.SetActive(false);
        ToolTipCardAnimControl.gameObject.SetActive(false);
        ReadyTextAnimControl.gameObject.SetActive(false);
    }

    public void PreGameMenuShow()
    {        
        Color c;
        if (!PanelGameImage.gameObject.activeSelf)
        {
            c = PanelGameImage.color; c.a = 0;
            PanelGameImage.color = c;
        }

        for (int i = 0; i < 2; i++)
        {
            //показать картинки, тексты перед показом, выкрутить альфу в ноль.
            c = CharsImages[i].color; c.a = 0;
            CharsImages[i].color = c;

            c = CharsShadowsImages[i].color; c.a = 0;
            CharsShadowsImages[i].color = c;            

            c = PlayersNames[i].color; c.a = 0;
            PlayersNames[i].color = c;

            //задать инфу об игроках в картинки,тексты
            int CharId = GameManager.GetPlayersCharId(i);
            CharsImages[i].sprite = Lib.instance.CharactersPortraitImages[CharId-1];
            PlayersNames[i].text = GameManager.GetPlayersName(i);
            PlayersChars[i].text = Lib.CharsDic[CharId].Name;

            UpdateShowedCharsData();

            CharsImages[i].gameObject.SetActive(true);
            CharsImages[i].transform.parent.gameObject.SetActive(true);
            CharsShadowsImages[i].gameObject.SetActive(true);
            PlayersNames[i].gameObject.SetActive(true);
            PlayersChars[i].gameObject.SetActive(true);
        }
        VSobj.SetActive(true);

        if (!PanelGameImage.gameObject.activeSelf)
        {
            Panel_Game.SetActive(true);
            Panel_MenuConnect.SetActive(false);
            Panel_SelectChar.SetActive(false);
            Panel_PanelWaiting.SetActive(false);
            CharsMiniAnimControl.gameObject.SetActive(false);
            CardsSelectPanelControl.gameObject.SetActive(false);
            DeskAnimControl.gameObject.SetActive(false);
            ReadyBtnAnimControl.gameObject.SetActive(false);
            ToolTipCardAnimControl.gameObject.SetActive(false);
            ReadyTextAnimControl.gameObject.SetActive(false);
        }                       
    }

    public void PreGameMenuCardsSelection()
    {
        for (int i = 0; i < 2; i++)
        {
            //скрыть картинки, тексты
            CharsImages[i].gameObject.SetActive(false);
            CharsImages[i].transform.parent.gameObject.SetActive(false);
            CharsShadowsImages[i].gameObject.SetActive(false);
            PlayersNames[i].gameObject.SetActive(false);
            PlayersChars[i].gameObject.SetActive(false);
        }
        VSobj.SetActive(false);        

        CharsMiniAnimControl.gameObject.SetActive(true);        
        CharsMiniAnimControl.Show();        
        ToolTipCardAnimControl.gameObject.SetActive(true);
        ToolTipCardAnimControl.Show();
        UpdateDeck(show: false);
        CardsSelectUpdate(show: true);
        DeskShowHideBtnControl.UpdateBtn(hide: true);

        //показ карт для набора в руку в начале игры
        //(здесь пока жестко захардкожено, что 3 классовые и 12 общих карт, при других значениях все сломается, надо будет переписать)
        CardInfo ci;        
        int[] cardIndex = { 0, 0 };
        foreach (Card c in GameManager.MyDeckPattern)
        {
            int groupIndex = c.Type == CardTypes.Ancient ? 1 : 0;

            ci = SelectingCards[groupIndex][cardIndex[groupIndex]];
            ci.Img.sprite = Lib.instance.CardsImgs[c.Id];
            ci.Id = c.Id;
            ci.Btn.interactable = true;

            cardIndex[groupIndex]++;
        }        
        
        SelectingBtnNext.interactable = false; //кнопка next будет активна, когда игрок 6 карт в руку возьмет        
    }

    public void EnableSelectCardButton(CardInfo ci)
    {
        int groupIndex = Lib.CardsDic[ci.Id].Type == CardTypes.Ancient ? 1 : 0;

        CardInfo ciFinded = SelectingCards[groupIndex].FirstOrDefault(x => x.Id == ci.Id);        

        if (ciFinded != null)
            ciFinded.Btn.interactable = true;        
    }

    //по окончании игры или при дисконнекте сбросить на начало все менюшки, карты и пр. игрока
    public void ResetAllOnEnd()
    {
        HandControl.ClearDeck();
        DeckControl.ClearDeck();
        DeskControl.ClearDesk();        
        ClearHPChangesText();
        ClearReactAndCopiesIcons();        
        HideTurnInfo(withFade: false);
        DeskShowHideBtnControl.UpdateBtn(hide: true);
        UpdateDeck(show: false);
        CardsSelectUpdate(show: false);
        ClearDeckTexts();
        VictoryLoseAnimControl.gameObject.SetActive(false);
    }

    public void SelectingBtnNextEnable(bool ok)
    {
        SelectingBtnNext.interactable = ok;
    }

    public void SelectingBtnNextOk()
    {
        if (!HandControl.CheckForDeckReady) return;

        SelectingBtnNextEnable(false);
        CardsSelectUpdate(show: false);
        HandControl.UpdateCardsOnClickToUseCard(); //изменить событие по клику на картах в руке        

        ReadyTextAnimControl.gameObject.SetActive(true);
        ReadyTextAnimControl.Show();

        GameManager.instance.MyDeckReady();
    }

    public void ShowGameDesk()
    {
        DeskAnimControl.gameObject.SetActive(true);
        DeskAnimControl.Show();

        ReadyBtnAnimControl.gameObject.SetActive(true);
        ReadyBtnAnimControl.Show();
        ReadyBtn.interactable = false; //кнопка ready будет активна, когда игрок выложит 3 карты на стол                

        CardsSelectPanelControl.gameObject.SetActive(false);
        ReadyTextAnimControl.gameObject.SetActive(false);

        for (int i = 0; i < 2; i++)
        {
            PlayersReactIconAnim[i].gameObject.SetActive(true);
            PlayersCopyIconObj[i].gameObject.SetActive(true);
        }           
    }

    public void HideGameDesk()
    {
        DeskAnimControl.Hide();
        ReadyBtnAnimControl.Hide();
        ReadyBtnAnimControl.Hide();
    }

    public void ReadyBtnEnable(bool ok)
    {
        ReadyBtn.interactable = ok;
    }

    public void SetPlayersNotReady(bool enableReadyBtn)
    {
        DeskControl.ShowReadyIcon(true, false);
        DeskControl.ShowReadyIcon(false, false);

        ReadyBtnEnable(enableReadyBtn);
    }


    public void SetPlayerReady(bool myPlayer)
    {
        if (myPlayer) ReadyBtnEnable(false);

        DeskControl.ShowReadyIcon(myPlayer, true);
    }

    public void RoundInfoHandler(string s)
    {
        RoundInfo.text = s;

        if (!RoundInfoTextAnimation.IsPlaying("FadeIn"))
            RoundInfoTextAnimation.Play("ScaleIn");
    }

    public void UpdateTurnInfo(string s)
    {        
            TurnInfo.text = s;
            if (!TurnInfoImgAnim.gameObject.activeSelf)
                TurnInfoImgAnim.gameObject.SetActive(true);

            TurnInfoImgAnim.Play("FadeIn");

            if (!TurnInfoTextAnim.gameObject.activeSelf)
                TurnInfoTextAnim.gameObject.SetActive(true);

            TurnInfoTextAnim.Play("FadeIn");    
    }

    public void HideTurnInfo(bool withFade = true)
    {
        TurnInfo.text = "";
        TurnInfoTextAnim.gameObject.SetActive(false);

        if (withFade)        
            TurnInfoImgAnim.Play("FadeOut");        
        else
            TurnInfoImgAnim.gameObject.SetActive(false);
    }
    
    public void ClearHPChangesText()
    {
        HPs_Damage[0].text = "";
        HPs_Damage[1].text = "";
        HPs_Heal[0].text = "";
        HPs_Heal[1].text = "";
    }    
    
    public void UpdatePlayerHPText(int player, int hp, int damage, int heal)
    {
        if (player < 0 || player > 1) return;

        HPs[player].text = hp.ToString();
        HPsAnim[player].Play("ScaleIn");

        if (damage > 0)
        {
            HPs_Damage[player].text = "-" + damage.ToString();
            HPs_DamageAnim[player].Play("ScaleIn");
        }
        else
        {
            HPs_Damage[player].text = "";
        }

        if (heal > 0)
        {
            HPs_Heal[player].text = "+" + heal.ToString();
            HPs_HealAnim[player].Play("ScaleIn");
        }
        else
        {
            HPs_Heal[player].text = "";
        }
    }

    public void ClearReactAndCopiesIcons()
    {
        for (int i = 0; i < 2; i++)
        {
            PlayersReactIconAnim[i].gameObject.SetActive(false);
            PlayersCopyIconObj[i].gameObject.SetActive(false);
        }
    }    

    public void UpdateReactIcon(int player, bool show)
    {
        if (player < 0 || player > 1) return;

        if (show && !PlayersReactIconAnim[player].gameObject.activeSelf)
        {
            PlayersReactIconAnim[player].gameObject.SetActive(true);
            PlayersReactIconAnim[player].Play("ScaleIn");
        }
        else if (!show && PlayersReactIconAnim[player].gameObject.activeSelf)        
            PlayersReactIconAnim[player].gameObject.SetActive(false);        
    }

    public void UpdateCopiesIconText(int player, int count)
    {
        if (player < 0 || player > 1) return;

        PlayersCopysText[player].text = count.ToString();
    }

    public void ReactIcoHandler(bool isEnemy, bool show)
    {
        UpdateReactIcon(isEnemy ? 1 : 0, show);
    }

    public void CopiesIcoHandler(bool isEnemy, int count)
    {
        UpdateCopiesIconText(isEnemy ? 1 : 0, count);
    }    

    public void CardsSelectUpdate(bool show)
    {
        if (show)
        {
            if (!CardsSelectPanelControl.gameObject.activeSelf)
                CardsSelectPanelControl.gameObject.SetActive(true);

            CardsSelectPanelControl.Show();
        }
        else
        {
            if (CardsSelectPanelControl.gameObject.activeSelf)
                CardsSelectPanelControl.Hide();
        }
    }

    public void UpdateDeck(bool show, List<Card> cards = null)
    {
        if (show)
        {
            DeckControl.gameObject.SetActive(true);            

            if (cards != null)
            {
                DeckControl.ClearDeck();

                foreach (Card c in cards)
                    DeckControl.AddCard(c.Id);

                DeckAnimControl.ForcedSetup();
            }

            DeckAnimControl.Show();
        }
        else            
            DeckAnimControl.Hide();
    }

    private void ClearDeckTexts()
    {
        foreach (var a in DecksIcoTexts)        
            foreach (Text t in a.Texts)
                t.text = "";        
    }

    public void DeckIcoTextsHandler(bool isEnemy, int handCount, int deckCount, int dropCount, int graveCount)
    {
        UpdateDeckIcoTexts(isEnemy ? 1 : 0, handCount, deckCount, dropCount, graveCount);
    }    

    private void UpdateDeckIcoTexts(int player, int handCount, int deckCount, int dropCount, int graveCount)
    {
        DecksIcoTexts[player].Texts[0].text = handCount.ToString();
        DecksIcoTexts[player].Texts[1].text = deckCount.ToString();
        DecksIcoTexts[player].Texts[2].text = dropCount.ToString();
        DecksIcoTexts[player].Texts[3].text = graveCount.ToString();
    }

    public void UpdateShowedCharsData()
    {
        for (int i = 0; i < 2; i++)
        {
            //внизу - я, вверху - оппонент.
            int CharId = GameManager.GetPlayersCharId(i);
            int playerId = i == 0 ? GameManager.MyId : GameManager.EnemyId;
            CharId = GameManager.GetPlayersCharId(playerId);
            PlayersNames[i + 2].text = GameManager.GetPlayersName(playerId);
            HPs[i].text = GameManager.GetHP(playerId).ToString();
            CharsImages[i + 2].sprite = Lib.instance.CharactersPortraitImages[CharId - 1];
        }
    }

    public void SelectCharOk()
    {
        if (GameManager.State == GameStates.SelectChar)
        {
            UpdateShowedCharsData();
            PreGameMenuCardsSelection();
            SelectCharPanelAnimControl.Hide();
            GameManager.State = GameStates.SelectCards;
            
            GameManager.instance.PlayerControlMy.MyAction(Actions.MyNewCharacter, GameManager.MyCharacterId.ToString());
        }
        else
            MenuConnect();
    }    

    public void PlayerBuffsViewHandler(bool isEnemy, List<Buff> buffs)
    {
        //добавляем к портретам игроков только те бафы, которые помечены, как не отображающиеся в ходах (значит отображать в портретах персонажей)
        List<Buff> buffsNew = buffs.Where(x => !x.IsOnTurnsView).ToList();        

        BuffsViewsChars[isEnemy ? 1 : 0].UpdateBuffs(buffsNew);
    }

    public void UpdateGameEndImg(int index)
    {        
        if (index < 0 || index >= Lib.instance.EndGameImages.Length) return;

        EndGameImg.sprite = Lib.instance.EndGameImages[index];
    }
}


