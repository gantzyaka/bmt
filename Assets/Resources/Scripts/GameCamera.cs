﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameCamera : MonoBehaviour {

    private static Vector2 _size;
    public static Vector2 Size
    {
        get { return _size; }
    }

    private void Start()
    {
        var cam = Camera.main;
        _size = new Vector2(cam.orthographicSize * cam.aspect, cam.orthographicSize);
    }
}
