﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameTypes;
using System.Linq;
using UnityEngine.UI;


public class DeskControl : MonoBehaviour {

    private enum colors { white = 0, green = 1, gray = 2 }

    [System.Serializable]
    public class CardsInfoArray
    {
        public CardInfo[] ciArray;
        public PlaceControl[] plArray;
    }
    public CardsInfoArray[] CardPlaces; //кнопки мест для карт на столе у каждого игрока (всего 5 карт показано: 1 предыдущего раунда, 3 текущего раунда, 1 следущего)

    [System.Serializable]
    public class ColorsArray
    {
        public Color[] colorsArray;
    }

    public ColorsArray[] PlaceButtonsColors; //цвета для кнопок (подсвечивать кнопки мест для карт, когда юзер может их выбирать)

    public event DelBool DeskFullEvent;

    public GameObject[] ReadyIcons;


    private void Awake()
    {
        CardInfo ci;
        PlaceControl pl;

        try
        {
            for (int j = 0; j < 2; j++)
            {
                for (int i = 0; i < 5; i++)
                {
                    ci = CardPlaces[j].ciArray[i];
                    ci.SetOnClick(CardInfoClicks.ClickDeskCard, i, j); //нажатие по месту для карты на столе

                    pl = CardPlaces[j].plArray[i];
                    pl.SetOnArrowClick(i); //нажатие по стрелке возле места для карты на столе

                    if (j == 0 && (i == 2 || i == 3))
                    {
                        pl.SetOnTokenClick(i);
                        pl.UpdateCopy(false);
                    }
                }

                ReadyIcons[j].SetActive(false);
            }
        }
        catch (System.Exception e)
        {
            Debug.LogException(e, this);
        }
    }

    private void Start()
    {
        ClearDesk();
    }



    public void EnablePlaces()
    {
        TurnPlaceBtnsColor(colors.green); //зеленая подсветка
    }

    public void DisablePlaces()
    {
        TurnPlaceBtnsColor(colors.white); //стандартная белая подсветка
    }

    private void TurnPlaceBtnsColor(colors color)
    {
        CardInfo ci;

        for (int i = 1; i < 4; i++)
        {
            ci = CardPlaces[0].ciArray[i]; //только нижние места для карт (поле игрока)

            if (color == DeskControl.colors.green && ci.Id != 0) continue; //для подсветки зеленым пустых мест пропускать места уже занятые картами                        

            ChangeButtonColors(ci.Btn, color);
        }
    }

    public void ClearDesk()
    {
        ResetCards();

        CardPlaces[0].ciArray[0].gameObject.SetActive(false);
        CardPlaces[1].ciArray[0].gameObject.SetActive(false);

        ClearTokens();
        ClearReadyIcons();
    }

    private void ResetCards()
    {
        CardInfo ci;

        for (int j = 0; j < 2; j++)
        {
            for (int i = 0; i < 5; i++)
            {
                ci = CardPlaces[j].ciArray[i];

                ci.Img.sprite = Lib.instance.CardPlaceImg;
                ChangeButtonColors(ci.Btn, (i != 0 && i != 4) ? colors.white : colors.gray);
                ci.Id = 0;

                CardPlaces[j].plArray[i].HideArrows();

                if (i == 2 || i == 3)
                    CardPlaces[j].plArray[i].UpdateCopy(false);
            }
        }
    }



    private void ChangeButtonColors(Button btn, colors color)
    {
        if (btn == null) return;

        ColorBlock colors = btn.colors;
        colors.normalColor = PlaceButtonsColors[(int)color].colorsArray[0];
        colors.highlightedColor = PlaceButtonsColors[(int)color].colorsArray[1];
        btn.colors = colors;

        btn.targetGraphic.color = colors.normalColor;
    }

    public void PutCardToPlace(bool myCard, int cardId, int placeId, bool isCopy = false, bool isHided = false)
    {
        if (cardId < 1 || cardId > Lib.CardsDic.Count || placeId < 1 || placeId > 3) return;

        if (myCard) //с моей стороны стола вижу свои карты целиком (кроме тех, ктр в реакт стадии меняю: isHided)
        {
            CardInfo ci = CardPlaces[0].ciArray[placeId];

            ci.Id = cardId;
            ci.Img.sprite = isHided ? Lib.instance.CardsImgs[0] : Lib.instance.CardsImgs[cardId];
            CardPlaces[0].plArray[placeId].UpdateCopy(isCopy);

            DisablePlaces();

            if (Lib.CardsDic[cardId].Effects.Any(x => x.Target == Target.Select))
                CardPlaces[0].plArray[placeId].ShowArrows();
            else
                CardPlaces[0].plArray[placeId].HideArrows();

            UpdateTokens();
            DeskFullEvent?.Invoke(CheckForFullDesk());
            ci.UpdateToolTip(); //обновить всплывающую карту-подсказку для этой карты
        }
        else //со стороны стола оппонента мне видно только как выкладываются рубашки
        {
            CardInfo ci = CardPlaces[1].ciArray[placeId];
            ci.Img.sprite = Lib.instance.CardsImgs[0];
        }
    }



    public void RemoveCardFromPlace(bool myCard, int placeId, bool hideMove = false)
    {
        if (placeId < 1 || placeId > 3) return;

        if (myCard)
        {
            PlaceControl pl = CardPlaces[0].plArray[placeId];
            CardInfo ci = CardPlaces[0].ciArray[placeId];

            ci.Id = 0;
            ci.Img.sprite = Lib.instance.CardPlaceImg;

            pl.HideArrows();
            if (pl.IsCopy) pl.UpdateCopy(false);

            UpdateTokens();
            DeskFullEvent?.Invoke(CheckForFullDesk());
            ci.HideToolTip(); //спрятать всплывающую карту-подсказку для этой карты
        }
        else if (!hideMove)
        {
            CardInfo ci = CardPlaces[1].ciArray[placeId];
            ci.Img.sprite = Lib.instance.CardPlaceImg;
        }
    }

    public bool CheckForFullDesk()
    {
        bool ok = true;

        for (int i = 1; i < 4; i++)
        {
            if (CardPlaces[0].ciArray[i].Id == 0) //если карты нет
            {
                ok = false;
                break;
            }
            else if (!CardPlaces[0].plArray[i].IsReady) //если у карты направление атаки не задано (если должно задаваться)
            {
                ok = false;
                break;
            }
        }

        return ok;
    }

    public void UpdateArrows(int placeId, Target target)
    {
        if (placeId < 1 || placeId > 3 || (target != Target.Me && target != Target.Enemy)) return;

        CardPlaces[0].plArray[placeId].SetTarget(target);

        DeskFullEvent?.Invoke(CheckForFullDesk());
    }

    private void UpdateTokens()
    {
        if (GameManager.MyUsedFreeToken && GameManager.MyTokens == 0) //нет больше токенов
        {
            ClearTokens(); //спрятать кнопки токенов
        }
        else //есть токены
        {
            for (int i = 2; i < 4; i++)
            {
                int cardId = CardPlaces[0].ciArray[i - 1].Id;

                if (cardId != 0 && CardPlaces[0].ciArray[i].Id == 0 && Lib.CardsDic[cardId].CanBeCopied(GameManager.MyCharacterId)) //текущее место не занято, в предыдущем есть карта для копирования
                    UpdateToken(i, GameManager.MyUsedFreeToken, GameManager.MyTokens);
                else
                    UpdateToken(i, false, -1);
            }
        }
    }

    private void UpdateToken(int index, bool useFreeToken, int tokensCount)
    {
        if (tokensCount == -1) //спрятать токен кнопку
        {
            CardPlaces[0].plArray[index].TokenBtn.image.sprite = Lib.instance.TokenImgs[0];
            CardPlaces[0].plArray[index].TokenText.text = "";
            CardPlaces[0].plArray[index].TokenBtn.gameObject.SetActive(false);
        }
        else if (!useFreeToken) //показать фритокен кнопку
        {
            CardPlaces[0].plArray[index].TokenBtn.image.sprite = Lib.instance.TokenImgs[0];
            CardPlaces[0].plArray[index].TokenText.text = "";
            CardPlaces[0].plArray[index].TokenBtn.gameObject.SetActive(true);
        }
        else //кнопка конечного числа токенов
        {
            CardPlaces[0].plArray[index].TokenBtn.image.sprite = Lib.instance.TokenImgs[1];
            CardPlaces[0].plArray[index].TokenText.text = tokensCount.ToString();
            CardPlaces[0].plArray[index].TokenBtn.gameObject.SetActive(true);
        }
    }

    private void ClearTokens()
    {
        for (int i = 2; i < 4; i++)
        {
            CardPlaces[0].plArray[i].TokenBtn.image.sprite = Lib.instance.TokenImgs[0];
            CardPlaces[0].plArray[i].TokenText.text = "";
            CardPlaces[0].plArray[i].TokenBtn.gameObject.SetActive(false);
        }
    }

    public void ClearReadyIcons()
    {
        ReadyIcons[0].SetActive(false);
        ReadyIcons[1].SetActive(false);
    }

    public void ShowReadyIcon(bool my, bool ready)
    {
        int player = my ? 0 : 1;
        ReadyIcons[player].SetActive(ready);
    }

    public void OpenEnemyAndMyCard(int index, int cardId, Target target, bool isCopy)
    {
        //карта оппонента
        CardInfo ci = CardPlaces[1].ciArray[index];
        ci.Id = cardId;
        ci.Img.sprite = Lib.instance.CardsImgs[cardId];
        CardPlaces[1].plArray[index].UpdateCopy(isCopy);

        if (Lib.CardsDic[cardId].Effects.Any(x => x.Target == Target.Select))
            CardPlaces[1].plArray[index].SetTarget(target);
        else
            CardPlaces[1].plArray[index].HideArrows();

        CardPlaces[1].plArray[index].PlayAnimCardOpen();

        //моя карта
        ci = CardPlaces[0].ciArray[index];
        ci.Img.sprite = Lib.instance.CardsImgs[ci.Id];
        CardPlaces[0].plArray[index].PlayAnimCardOpen();
    }

    public void HideMyCards(int turn) //мои карты на столе перевернуть обложкой вверх
    {
        CardInfo ci;

        for (int i = turn + 1; i < 4; i++)
        {
            ci = CardPlaces[0].ciArray[i];
            ci.Img.sprite = Lib.instance.CardsImgs[0];
        }
    }


    public void MoveDesk(Place[,] deskMap, bool showEarly, bool roundIsCurrent, bool roundIsPrevious, int selectedCard, int currentTurn, bool isPutStage, bool ImNotReady, bool myGatherReady)
    {
        if (deskMap == null) return;

        CardInfo ci;
        Place pl;
        colors color;

        for (int j = 0; j < 2; j++)
        {
            for (int i = 0; i < 5; i++)
            {
                ci = CardPlaces[j].ciArray[i];
                pl = deskMap[j, i];
                color = (i != 0 && i != 4) ? colors.white : colors.gray;

                if (i == 0) ci.gameObject.SetActive(showEarly); //спрятать/показать карты с предыдущего раунда (на первом раунде не надо показывать)

                if (pl != null && pl.Card != null)
                {
                    if (((roundIsCurrent && i > 0 && i < 4 && i > currentTurn) || (roundIsPrevious && i == 4 && j == 1 && currentTurn < 1)) && (!isPutStage || (isPutStage && (j == 1 || !ImNotReady)))) //закрытые карты
                    //ЕСЛИ ((раунд текущий И ход [1-3] И ход > текущего хода) ИЛИ (раунд пред-текущий И ход = 4 И карты врага И текущий ход < 1) И (не стадия выкладки (ИЛИ) стадия выкладки  и (карты врага ИЛИ я готов))
                    {
                        ci.Img.sprite = Lib.instance.CardsImgs[0];

                        if (j == 0 && Lib.CardsDic[pl.Card.Id].Effects.Any(x => x.Target == Target.Select)) //на своих картах рубашкой кверху показывать стрелки
                            CardPlaces[j].plArray[i].SetTarget(pl.Target);
                        else
                            CardPlaces[j].plArray[i].HideArrows();

                        ci.Id = j != 1 ? pl.Card.Id : 0; //только в своих закрытых картах знаю что лежит
                        CardPlaces[j].plArray[i].UpdateCopy(j != 1 ? pl.IsCopy : false); //только на своих закрытых картах вижу иконки копий
                    }
                    else //открытые карты
                    {
                        ci.Img.sprite = Lib.instance.CardsImgs[pl.Card.Id];
                        ci.Id = pl.Card.Id;

                        if (Lib.CardsDic[pl.Card.Id].Effects.Any(x => x.Target == Target.Select))
                            CardPlaces[j].plArray[i].SetTarget(pl.Target);
                        else
                            CardPlaces[j].plArray[i].HideArrows();

                        CardPlaces[j].plArray[i].UpdateCopy(pl.IsCopy);
                    }
                }
                else
                {
                    if (roundIsCurrent && !isPutStage && j == 1 && i > 0 && i < 4 && i > currentTurn) //не показываем, как чужие карты двигаются в реакт стадии, тут всегда лежит карта рубашкой вверх
                        ci.Img.sprite = Lib.instance.CardsImgs[0];
                    else
                        ci.Img.sprite = Lib.instance.CardPlaceImg;

                    ci.Id = 0;

                    //для текущего раунда если у игрока выделена карта в руке - подсветить зеленым пустые места
                    if (roundIsCurrent && j == 0 && i > 0 && i < 4 && selectedCard > -1) color = colors.green;

                    CardPlaces[j].plArray[i].HideArrows();
                    CardPlaces[j].plArray[i].UpdateCopy(false);
                }

                ChangeButtonColors(ci.Btn, color);
            }
        }

        UpdateTokens(); //кнопки копирования карт

        if (myGatherReady) //только если карты в руку добрал, влиять движениями стола на кнопку "готов"
            DeskFullEvent?.Invoke(roundIsCurrent && ImNotReady && CheckForFullDesk()); //кнопку можно жмякать только в текущем раунде, и если не жмякал еще её в этом раунде

        //при движениях стола обновляются бафы во вьюхах
        GameManager.UpdatePlayersBuffsView();
    }

    public void BuffsOnTurnViewHandler(bool isEnemy, List<Buff> buffs)
    {
        int turn = GameManager.CurrentTurn;
        int round = GameManager.CurrentRound;
        int roundSee = GameManager.MyRoundSee;
        if (turn == -1 || round == -1 || roundSee == -1) return;

        int playerId = isEnemy ? 1 : 0;

        //показываем на столе у карт только те бафы, которые помечены, как отображающиеся в ходах
        List<Buff> buffsNew = buffs.Where(x => x.IsOnTurnsView).ToList();

        //список мест карт с распределёнными по ним бафами
        List<Buff>[] distributedBuffs = new List<Buff>[5];
        for (int i = 0; i < 5; i++) { distributedBuffs[i] = new List<Buff>(); }

        //распределение бафов по местам
        if ((round - roundSee) <= 1) //может видеть бафы только в актуальном раунде либо ему предшествующем
        {
            //сперва бафы распределяются так, как если игрок смотрит актуальный раунд
            foreach (Buff b in buffsNew)
            {
                switch (b.Duration.DurType)
                {
                    case DurationTypes.TurnsLeft:
                        int activationOffset = b.GetActivationOffset(GameManager.GameStage);     

                        int firstIndex = turn + activationOffset;
                        int lastIndex = firstIndex + b.Duration.Turns;

                        if (b.ActivationTime == ActivationTimeTypes.AtEnd)
                            firstIndex = lastIndex - 1;

                        for (int i = firstIndex; i < lastIndex && i < 5; i++)
                            distributedBuffs[i].Add(b);
                        break;

                    case DurationTypes.EndRound:
                        int offset = GameManager.GameStage == GameStages.Turn ? 0 : 1;                        
                        for (int i = turn + offset; i < 4; i++)
                            distributedBuffs[i].Add(b);
                        break;
                }
            }

            //если смотрит раунд, предшествующий актуальному, то видит только "бафы начала актуального раунда" в конце стола
            if (round - roundSee == 1)
            {                
                distributedBuffs[4] = distributedBuffs[1];
                distributedBuffs[1] = new List<Buff>();

                for (int i = 0; i < 4; i++)
                    distributedBuffs[i].Clear();
            }
        }        
        
        //обновление бафов в местах у карт согласно списку распределения
        for (int i = 0; i < 5; i++)        
            CardPlaces[playerId].plArray[i].Buffs.UpdateBuffs(distributedBuffs[i]);        
    }
}
