﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeskShowHideBtn : MonoBehaviour {

    private Button Btn;        

    public Sprite ImgDeskShow;
    public Sprite ImgDeskHide;    

    private void Awake()
    {
        Btn = this.GetComponent<Button>();
        UpdateBtn(hide: true);
    }    

    public void UpdateBtn(bool hide = false)
    {
        if (hide)
        {
            this.gameObject.SetActive(false);
            return;
        }

        if (!this.gameObject.activeSelf)
            this.gameObject.SetActive(true);
        
        bool showed = GameManager.MyDeskSee;

        if (showed)        
            Btn.image.sprite = ImgDeskHide;        
        else        
            Btn.image.sprite = ImgDeskShow;        
    }
}
