﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;


public class Racket : NetworkBehaviour {

    [SerializeField] private float speed = 0.1f;
    [SyncVar(hook = "OnPointsChanged")] private int _points; //синхронизирумоя переменноя
    public int Points
    {
        get { return _points; }
        set
        {
            if (value > 0 && _points != value)
                _points = value;
        }
    }

    private void OnPointsChanged(int points)
    {
        //CustomNetworkManager.instance.UpdateUI(this, _points);
    }


    private void FixedUpdate()
    {
        if (!isLocalPlayer) //если не наш персонаж, то не управляем им
            return; 

        if (Input.GetKey(KeyCode.UpArrow) && transform.position.y < GameCamera.Size.y - transform.localScale.y / 2)
            transform.Translate(0, speed, 0);
        else if (Input.GetKey(KeyCode.DownArrow) && transform.position.y > -GameCamera.Size.y + transform.localScale.y / 2)
            transform.Translate(0, -speed, 0);
    }

    public override void OnStartLocalPlayer()
    {
        GetComponent<SpriteRenderer>().color = Color.red;
        base.OnStartLocalPlayer();
    }
}
