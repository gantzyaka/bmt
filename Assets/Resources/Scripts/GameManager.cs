﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameTypes;
using System;
using System.Linq;

public class GameManager : MonoBehaviour {
        
    public static GameManager instance { get; private set; }    

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
    }

    private static Player MyPlayer = new Player();
    public static int MyId = -1;
    private static Player[] Pls = new Player[2]; //0 = хост, 1 - клиент. при коннекте игроков определяется кто где.    

    public PlayerControl PlayerControlMy;
    public PlayerControl PlayerControlEnemy;
    public static GameStates State = GameStates.Off;
    public static Game Game;
    private static int TurnStepToUpdate;

    private static event DelPlaySFX OnPlaySFX; //событие, когда нужно проиграть звук
    private static event DelPlayMusic OnPlayMusic; //событие, когда нужно сменить музыку

    public void Start()
    {
        //загрузка инфы игрока
        MyPlayer.SetName(PlayerPrefs.GetString("MyName", "MyName"));
        MyPlayer.SetChar(PlayerPrefs.GetInt("MyCharacterId", 1));

        MenuManager.instance.InputName.text = MyPlayer.Name;
        MenuManager.instance.UpdateSelectedChar(MyPlayer.CharId);

        OnPlaySFX += SoundManager.instance.PlaySFXHandler;
        OnPlayMusic += SoundManager.instance.PlayMusicHandler;
    }

    public static void GetSomeShit(ref string s0, ref string s1)
    {
        s0 += "MyPlayer: " + MyPlayer.Name + " ; MyId = " + MyId + "\n";
        s0 += "EnemyPlayer: " + EnemyPlayer.Name + " ; EnemyId = " + EnemyId + "\n\n";

        for (int i = 0; i < 2; i++)
        {
            string s = "";

            string hand = "";
            foreach (Card c in Pls[i].Hand)
                hand += c.Name + " ";            

            string deck = "";
            foreach (Card c in Pls[i].Deck)
                deck += c.Name + " ";            

            string grave = "";
            foreach (Card c in Pls[i].Grave)
                grave += c.Name + " ";            

            string drop = "";
            foreach (Card c in Pls[i].Drop)
                drop += c.Name + " ";            

            string deckPattern = "";
            foreach (Card c in Pls[i].DeckPattern)
                deckPattern += c.Name + " ";

            string buffs = "";
            foreach (Buff b in Pls[i].Buffs)
                buffs += Lib.BuffNameDic[b.BuffType] + " ";

            s += "Player " + i + ": " + Pls[i].Name;            
            s += "\nCharId = " + Pls[i].CharId;
            s += "\nIsEnemy = " + Pls[i].IsEnemy;
            s += "\nLastReact == null = " + (Pls[i].LastReact == null);
            s += "\nTokens = " + Pls[i].Tokens;
            s += "\nGatherCards = " + Pls[i].GatherCards;
            s += "\nIsReady = " + Pls[i].IsReady;

            s += "\n\nBuffs = " + Pls[i].Buffs.Count + "\n";
            s += buffs;
            s += "\n\nHand: " + Pls[i].Hand.Count + "\n";
            s += hand;
            s += "\n\nDeck: " + Pls[i].Deck.Count + "\n";
            s += deck;
            s += "\n\nDeckPattern: " + Pls[i].DeckPattern.Count + "\n";
            s += deckPattern;
            s += "\n\nDrop: " + Pls[i].Drop.Count + "\n";
            s += drop;
            s += "\n\nGrave: " + Pls[i].Grave.Count + "\n";
            s += grave;

            if (i == 0)
                s0 += s;
            else
                s1 += s;
        }        
    }

    public static bool MyGatherReady
    {
        get { return MyPlayer.GatherCards == -1; }
    }

    public static List<Card> MyDeck
    {
        get { return MyPlayer.Deck; }
    }

    public static List<Card> MyDeckPattern
    {
        get { return MyPlayer.DeckPattern; }
    }

    public static bool MyDeskSee
    {
        get { return MyPlayer.DeskSee; }
    }

    public static Player OtherPlayer(Player p)
    {
        return p == Pls[0] ? Pls[1] : Pls[0];
    }

    public static Player EnemyPlayer
    {
        get { return Pls[EnemyId]; }
    }

    public static int MyCharacterId
    {
        get { return MyPlayer.CharId; }
    }

    public static void SetMyCharacter(int a)
    {
        MyPlayer.SetChar(a);
    }

    public static void SetMyName(string name)
    {
        MyPlayer.SetName(name);
    }

    public static string MyName
    {
        get { return MyPlayer.Name; }
    }

    public static void SetMeHost()
    {
        MyPlayer.SetHost(true);
    }

    public static void ResetMeHost()
    {
        MyPlayer.SetHost(false);
    }

    public static bool isHost
    {
        get { return MyPlayer.IsHost; }
    }

    //только у хоста вызывается
    public static void SetConnectionId(int player, int id) //хост в 0 падает, клиент в 1
    {
        if (MyId == player)
        {
            MyPlayer.SetConnectionId(id);
            Pls[player] = MyPlayer;
        }
        else
        {
            Pls[player] = new Player();
            Pls[player].SetConnectionId(id);
        }
    }

    public static int EnemyConnectionId
    {
        get { return Pls[EnemyId].ConnectionId; }
    }

    public static int EnemyId
    {
        get { return MyId == 0 ? 1 : 0; }
    }

    public static string EnemyName
    {
        get { return Pls[EnemyId] != null ? Pls[EnemyId].Name : "NoName"; }
    }


    public static void SetPlayerParams(int charId, string name, int id)
    {
        Debug.Log("SetPlayerParams: id = " + id + " ; chaId = " + charId + " ; name = " + name);

        //коннекшн айди должен сохраниться при обновленном объекте противника
        //он нужен, чтобы при дисконнекте клиента смотреть по нему, кто это был такой, и если это был оппонент, то выкидывать хоста на главный экран со сбросом игры
        //(когда игроки меняются в запуске игры: то один хост, то второй, - объект игрока оппонента каждый раз создается новый для хоста, а коннекшн айди у них обоих при этом не меняется)
        int oldConId = (Pls[id] != null && Pls[id].ConnectionId != -1) ? Pls[id].ConnectionId : -1;

        Pls[id] = id == MyId ? MyPlayer : new Player();

        if (oldConId != -1)
            Pls[id].SetConnectionId(oldConId);        

        Pls[id].SetChar(charId); //здесь же дефолтная колода задается по выбранному персонажу, происходит сброс всех старых колод, нанесенного дамага, HP, токенов
        Pls[id].SetName(name);
        Pls[id].SetEnemy(id != MyId);
        Pls[id].SetArrayId(id);
    }

    public static int GetPlayersCharId(int id)
    {
        return Pls[id].CharId;
    }

    public static string GetPlayersName(int id)
    {
        return Pls[id].Name;
    }    

    public static void ResetPlayer(int id)
    {
        Pls[id] = new Player();
    }

    public static int GetHP(int id)
    {
        return Pls[id].HP;
    }

    public static int MyTokens
    {
        get { return MyPlayer.Tokens; }
    }

    public static bool MyUsedFreeToken
    {
        get { return MyPlayer.RoundWithUsedFreeToken > -1; }
    }

    public static bool SeeCurrentRound
    {
        get { return Game != null && MyPlayer.RoundSee == Game.Round; }
    }

    public static int CurrentTurn
    {
        get { return Game != null ? Game.Turn : -1; }
    }

    public static int CurrentRound
    {
        get { return Game != null ? Game.Round : -1; }
    }

    public static int MyRoundSee
    {
        get { return MyPlayer.RoundSee; }
    }

    public static GameStages GameStage
    {
        get { return Game != null ? Game.Stage : GameStages.Put; }
    }

    
    //ЛОГИКА ИГРЫ    
    //обновление игры по результату хода (метод вызывается сперва у хоста, потом у клиента - клиент получает результат от своих команд и команд хоста)
    public static void UpdateGame(int playerId, Actions act, string mes)
    {
        bool IsMyAction = playerId == MyId;
        Player p = Pls[playerId];

        string[] ss = mes?.Split(',');        
        bool isReactStage = Game?.Stage == GameStages.React;

        switch (act)
        {
            case Actions.MyDeck:
                p.CreateHand(ss);
                p.CreateDeckFromDeckPatternExceptHand();
                OnPlaySFX(IsMyAction ? SFXType.Ready : SFXType.ReadyOther);
                if (isHost)
                {
                    //ответ клиенту от хоста о том, что произошло                    
                    instance.PlayerControlMy.CmdResultSend(playerId, act, mes);

                    if (!Pls[0].IsReady && !Pls[1].IsReady && Pls[0].Hand.Count == 6 && Pls[1].Hand.Count == 6) //если у всех карты набраны, и обновились данные игроков после окончания предыдущей игры (IsReady = false), то старт новой игры
                    {
                        instance.PlayerControlMy.CmdResultSend(MyId, Actions.StartGame, null); //отправка совего результата о начале игры клиенту
                        UpdateGame(MyId, Actions.StartGame, null); //начало игры у себя                        
                    }
                }
                break;

            case Actions.StartGame:
                MenuManager.instance.ShowGameDesk();
                GameManager.State = GameStates.Game;
                Game = new Game();
                Game.SubscribeOnEvents(instance.NextTurnStep, MenuManager.instance.RoundInfoHandler); //подписка на шаги выполнения хода, на обновления состояний раунда                
                MyPlayer.OnGatherFull += MenuManager.instance.ReadyBtnEnable; //подписка кнопки "Готов" на выбранное моим игроком количество карт (3) для добора из колоды в конце каждого раунда

                //подписка на изменения у игроков: в колодах/сбросе/могиле, в возможности менять карту в рекат-стадии, в количестве доступных копирований карт, в бафах
                MyPlayer.SubscribeOnEvents(MenuManager.instance.DeckIcoTextsHandler, MenuManager.instance.ReactIcoHandler, MenuManager.instance.CopiesIcoHandler, MenuManager.instance.PlayerBuffsViewHandler, MenuManager.instance.DeskControl.BuffsOnTurnViewHandler);
                EnemyPlayer.SubscribeOnEvents(MenuManager.instance.DeckIcoTextsHandler, MenuManager.instance.ReactIcoHandler, MenuManager.instance.CopiesIcoHandler, MenuManager.instance.PlayerBuffsViewHandler, MenuManager.instance.DeskControl.BuffsOnTurnViewHandler);

                //в начале игры сразу активируются бафы от споcобностей персонажей, если есть (у вампира - x2, у демоника - манарам)
                AddBuffsFromCharAbilitiesAtTurnEnd();
                UpdatePlayersBuffsView();

                OnPlayMusic(MusicType.Match);
                break;

            case Actions.PutCardToDesk:
                if (ss.Length != 2) break;
                if (p.IsReady ) break;
                if (IsMyAction && !MyGatherReady) break;

                int selectedCard = Int32.Parse(ss[0]);
                int selectedPlace = Int32.Parse(ss[1]);
                Place place = Game.GetPlace(playerId, selectedPlace - 1);
                bool ok = false;

                if (isReactStage) //ограничения на действия с картами в реакт стадии
                {
                    if (selectedPlace == 2 && Game.GetPlace(playerId, 2).IsCopy) break; //нельзя источник копии менять
                    if (p.LastReact != null && (p.LastReact.IsDone || selectedPlace - 1 != p.LastReact.IndexFrom || p.LastReact.AtRound != Game.Round)) break; //можно трогать только то место, откуда ранее карту убрал
                }

                if (selectedCard > -1) //выложить выбранную карту на стол / заменить карту на столе выбранной с руки
                {
                    Card c = p.Hand[selectedCard];

                    if (place.Card == null) //положить карту в пустое место стола
                    {
                        place.SetCard(c);
                        p.Hand.Remove(c);
                       
                        if (isReactStage && p.LastReact != null && p.LastReact.CardId == c.Id) //назад вернул карту, которую запоминал, что убрал
                            p.ReactCommit(null); //снова доступна замена карты

                        if (MyPlayer.RoundSee == Game.Round) //показывать движение карт актуального раунда игроку только тогда, когда у него открыт этот раунд на столе
                            MenuManager.instance.DeskControl.PutCardToPlace(IsMyAction, c.Id, selectedPlace, isHided: isReactStage);

                        if (IsMyAction)
                            MenuManager.instance.HandControl.RemoveCard(p.SelectedCard);

                        p.ResetSelecting();
                        ok = true;
                    }
                    else //заменить карту в указанном месте стола
                    {
                        bool isCopy = place.IsCopy;

                        Card tempCard = place.Card;
                        place.SetCard(c);

                        if (isCopy) //если карту-копию заменяет, то не возвращать её в руку
                        {
                            p.Hand.Remove(c);
                            p.UseTokens(decrease: false, round: Game.Round);
                        }
                        else                        
                            p.Hand[selectedCard] = tempCard;                        

                        if (isReactStage)
                        {
                            if (p.LastReact == null) //в реакт-стадии замену карты запоминаем в игроке, чтоб не мог больше менять карты, или мог вернуть назад карту, которую заменил
                                p.ReactCommit(new ReactCommit(tempCard.Id, selectedPlace - 1, Game.Round));                            
                            else if (p.LastReact.CardId == c.Id) //назад вернул карту, которую запоминал, что убрал
                                p.ReactCommit(null); //снова доступна замена карты                             
                        }

                        if (MyPlayer.RoundSee == Game.Round) //показывать движение карт актуального раунда игроку только тогда, когда у него открыт этот раунд на столе
                            MenuManager.instance.DeskControl.PutCardToPlace(IsMyAction, c.Id, selectedPlace, isHided: isReactStage);

                        DeleteDuplicatedCardsFromPlace(selectedPlace, playerId, p, IsMyAction); //удалить дубликаты справа от бывшей карты, если они есть

                        if (IsMyAction)
                        {
                            if (isCopy)
                                MenuManager.instance.HandControl.RemoveCard(p.SelectedCard); //если карту-копию заменял, то не возвращать её в руку                            
                            else
                                MenuManager.instance.HandControl.ChangeCard(selectedCard, tempCard.Id);
                        }

                        p.ResetSelecting();
                        ok = true;
                    }
                }
                else if (place.Card != null) //убрать карту с указанного места стола и (если карта - не копия) добавить себе в руку 
                {
                    bool isCopy = place.IsCopy;

                    Card c = place.Card;

                    if (!isCopy)
                        p.Hand.Add(c);

                    place.SetCard(null);

                    if (isCopy)
                        p.UseTokens(decrease: false, round: Game.Round);

                    if (MyPlayer.RoundSee == Game.Round) //показывать движение карт актуального раунда игроку только тогда, когда у него открыт этот раунд на столе
                        MenuManager.instance.DeskControl.RemoveCardFromPlace(IsMyAction, selectedPlace, hideMove: isReactStage); //hideMove: в рекат стадии не показывать, как оппонент меняет карты

                    DeleteDuplicatedCardsFromPlace(selectedPlace, playerId, p, IsMyAction); //удалить дубликаты справа от бывшей карты, если они есть

                    //убранную карту в реакт стадии фиксируем, чтоб не мог менять другие карты, или мог вернуть назад карту, которую убрал
                    if (isReactStage && p.LastReact == null)                    
                        p.ReactCommit(new ReactCommit(c.Id, selectedPlace - 1, Game.Round));

                    if (IsMyAction && !isCopy)
                        MenuManager.instance.HandControl.AddCard(c.Id);

                    ok = true;
                }

                if (ok)
                {
                    p.UpdateDecksIcoTexts(isReactStage: isReactStage, virtualHandDec: Game.GetCopiesCountAtCurrentRound(playerId));

                    if (IsMyAction || (!IsMyAction && !isReactStage))
                        OnPlaySFX(SFXType.CardPlacement);
                }

                if (isHost && ok) //если команда выполнилась на хосте - ответ клиенту от хоста о том, что произошло                                    
                    instance.PlayerControlMy.CmdResultSend(playerId, act, mes);                
                break;

            case Actions.SetCardTarget:
                if (p.IsReady) break;                
                if (ss.Length != 2) break;
                if (IsMyAction && !MyGatherReady) break;

                int placeId = Int32.Parse(ss[0]);
                int arrowId = Int32.Parse(ss[1]);
                Place plc = Game.GetPlace(playerId, placeId - 1);

                if (isReactStage && placeId <= Game.Turn) break; //только стрелки у карт еще не разыгранных ходов можно менять

                plc.SetTarget(arrowId);

                if (IsMyAction)
                {
                    MenuManager.instance.DeskControl.UpdateArrows(placeId, plc.Target);
                    OnPlaySFX(SFXType.CardTarget);
                }

                if (isHost) //если команда выполнилась на хосте - ответ клиенту от хоста о том, что произошло                                    
                    instance.PlayerControlMy.CmdResultSend(playerId, act, mes);                
                break;

            case Actions.CopyCard:                    
                if (p.IsReady || !p.HasTokens) break;                    
                if (IsMyAction && !MyGatherReady) break;                    
                if (string.IsNullOrEmpty(mes)) break;

                int placeIdOfCopy = Int32.Parse(mes);
                Place placeOfCopy = Game.GetPlace(playerId, placeIdOfCopy - 1);
                Place placeOfSource = Game.GetPlace(playerId, placeIdOfCopy - 2);                    

                if (placeOfCopy.Card != null || placeOfSource.Card == null || !placeOfSource.Card.CanBeCopied(p.CharId)) break;
                if (isReactStage && (p.LastReact == null || p.LastReact.IsDone || p.LastReact.AtRound != Game.Round || placeIdOfCopy - 1 != p.LastReact.IndexFrom)) break; //в рекат стадии можно копировать только в том месте, откуда ранее карту убрал и только на том же ходу                    

                placeOfCopy.SetCard(placeOfSource.Card, isCopy: true);
                p.UseTokens(decrease: true, round: Game.Round);

                if (isReactStage && p.LastReact.CardId == placeOfSource.Card.Id) //назад вернул карту, которую запоминал, что убрал                
                    p.ReactCommit(null); //снова доступна замена карты

                if (MyPlayer.RoundSee == Game.Round) //показывать движение карт актуального раунда игроку только тогда, когда у него открыт этот раунд на столе
                    MenuManager.instance.DeskControl.PutCardToPlace(IsMyAction, placeOfSource.Card.Id, placeIdOfCopy, isCopy: true, isHided: isReactStage);
                
                p.UpdateDecksIcoTexts(isReactStage: isReactStage, virtualHandDec: Game.GetCopiesCountAtCurrentRound(playerId));

                if (IsMyAction || (!IsMyAction && !isReactStage))
                    OnPlaySFX(SFXType.CardPlacement);

                if (isHost) //если команда выполнилась на хосте - ответ клиенту от хоста о том, что произошло                                    
                    instance.PlayerControlMy.CmdResultSend(playerId, act, mes);                
                break;

            case Actions.ImReady:
                if (p.IsReady || !CheckForDeckReady(playerId)) break;                
                if (IsMyAction && !MyGatherReady) break;

                p.SetIsReady(true);
                MenuManager.instance.SetPlayerReady(IsMyAction);

                if (IsMyAction) //если моя команда - задвинуть карты в руке + сбросить выделение
                {
                    if (MyPlayer.SelectedCard > -1)
                        MenuManager.instance.HandControl.DeselectCard(MyPlayer.SelectedCard);

                    MenuManager.instance.DeskControl.HideMyCards(Game.Turn);
                    MyPlayer.ResetSelecting();
                }
                
                OnPlaySFX(IsMyAction ? SFXType.Ready : SFXType.ReadyOther);

                if (isHost) //если команда выполнилась на хосте - ответ клиенту от хоста о том, что произошло
                {                    
                    instance.PlayerControlMy.CmdResultSend(playerId, act, mes);

                    if (MyPlayer.IsReady && EnemyPlayer.IsReady) //все готовы - старт действия хода
                    {
                        instance.PlayerControlMy.CmdResultSend(MyId, Actions.NextTurn, null); //отправка своего результата о том, что начался обсчет хода клиенту
                        UpdateGame(MyId, Actions.NextTurn, null); //начало обработки хода у себя
                    }
                }
                break;

            case Actions.NextTurn:
                bool nextRound = Game.NextTurn();                

                if (!nextRound)
                    MenuManager.instance.HideTurnInfo();

                if (!SeeCurrentRound) //сдвинуть стол на актуальный раунд
                {
                    MyPlayer.SetRoundSee(Game.Round);
                    MenuManager.instance.DeskControl.MoveDesk(Game.GetDeskMap(MyId), showEarly: Game.Round > 0, roundIsCurrent: true, roundIsPrevious: false, selectedCard: MyPlayer.SelectedCard, currentTurn: Game.Turn - 1, isPutStage: Game.Stage == GameStages.Put, ImNotReady: !MyPlayer.IsReady, myGatherReady: MyGatherReady); //здесь currentTurn = turn - 1, чтобы раньше времени карты текущего хода не открыть (откроется анимацией в Actions.NextTurnStep)
                    OnPlaySFX(SFXType.MoveTable);
                }   

                ResetPlayersByTurn(); //сброс отображаемых изменений в HP игроков                                

                if (nextRound)
                {
                    ResetPlayersByRound(Game.Round); //сброс использованных копирований и замен (каждый через раунд от использования сбрасывается)
                    ResetPlayersReady(enableReadyBtn: false);

                    if (!MyGatherReady)
                    {
                        MenuManager.instance.DeskShowHideBtnControl.UpdateBtn();
                        MenuManager.instance.UpdateDeck(show: true, cards: MyDeck);
                    }
                }   

                MenuManager.instance.UpdateReactIcon(1, EnemyPlayer.LastReact == null); //в начале хода показываем, потратил ли противник свою замену в прошлой реакт-стадии
                if (nextRound)
                {
                    //есть ли проигравшие (карт недостаточно, чтоб в раунд выложить) -> конец игры
                    if (CheckForEnd(checkForEnoughCards: true)) 
                        break; 
                }

                if (isHost && !nextRound)
                    Game.NextTimer(); //таймер стадий хода только у хоста тикает                
                break;

            case Actions.NextTurnStep:                
                int step = Int32.Parse(mes);

                switch (step)
                {
                    case 1: //открываем карты текущего хода: свою и чужую
                        Place ePlc = Game.GetPlace(EnemyId, Game.Turn - 1);
                        MenuManager.instance.DeskControl.OpenEnemyAndMyCard(Game.Turn, ePlc.Card.Id, ePlc.Target, ePlc.IsCopy);
                        if (ePlc.IsCopy) //если у оппонента вскрылась копия
                        {
                            int closedCopies = Game.GetCopiesCountAtCurrentRound(EnemyId, untilCurrentTurn: true); //количество еще не открытых копий до текущего открытого хода у оппонента
                            EnemyPlayer.UpdateDecksIcoTexts(isReactStage: false, virtualHandDec: closedCopies); //обновить счетчик карт оппонента
                            EnemyPlayer.CopiesInvoke(closedCopies); //обновить счетчик доступных копий оппонента
                        }
                        OnPlaySFX(SFXType.CardReveal);
                        break;

                    case 2: //выполняем действия с карт
                        CalculateTurn();                        
                        break;

                    case 3: //завершаем ход: реакт стадия, либо начало нового раунда
                        bool isLastTurn = Game.Turn == 3;

                        //Пересчет срока действия бафов у игроков под конец каждого хода
                        ApplyPlayersBuffsDuration(dur: isLastTurn ? DurationTypes.EndRound : DurationTypes.TurnsLeft); 

                        if (!isLastTurn) //реакт-стадия (только не на последнем ходу раунда)
                        {
                            Game.NextReact();                            
                            ResetPlayersReady(enableReadyBtn: true);
                            OnPlaySFX(SFXType.React);
                        }
                        else
                        {
                            ReturningAndDevidingPlayedCards(); //возврат у игроков карт из сброса в колоду, отправка в сбросы и могилы карт с текущего раунда
                            UpdateGame(MyId, Actions.NextTurn, null); //сразу следущий ход
                        }

                        //добавить бафы от абилок персонажей, если есть
                        AddBuffsFromCharAbilitiesAtTurnEnd();

                        //обновить отображение бафов у игроков (обязательно только после того, как пересчиталась длительность бафов, и стадия игры сменилась на следущую)
                        UpdatePlayersBuffsView(); 
                        break;
                }

                if (isHost) //если команда выполнилась на хосте - ответ клиенту от хоста о том, что произошло                                    
                    instance.PlayerControlMy.CmdResultSend(playerId, act, mes);                
                break;

            case Actions.SetGatherCards:                
                if (ss == null || ss.Length == 0) break;
                if (Game.Stage != GameStages.Put || p.IsReady || p.GatherCards == -1) break;
                List<Card> cards = p.GatherCardsFromDeckToHand(ss);                
                if (IsMyAction)
                {
                    MenuManager.instance.DeskShowHideBtnControl.UpdateBtn(hide: true);
                    MenuManager.instance.UpdateDeck(show: false);
                    MenuManager.instance.HandControl.AddCards(cards);
                    OnPlaySFX(SFXType.CardTarget);
                }
                p.UpdateDecksIcoTexts(isReactStage: false);
                if (isHost) //если команда выполнилась на хосте - ответ клиенту от хоста о том, что произошло                                    
                    instance.PlayerControlMy.CmdResultSend(playerId, act, mes);                
                break;

            case Actions.MyNewCharacter:
                int newChar = Int32.Parse(mes);
                Pls[playerId].Reset();
                Pls[playerId].SetChar(newChar); //здесь же дефолтная колода задается по выбранному персонажу, происходит сброс всех старых колод, нанесенного дамага, HP, токенов                
                MenuManager.instance.UpdateShowedCharsData();
                UpdatePlayersBuffsView();
                if (isHost) //если команда выполнилась на хосте - ответ клиенту от хоста о том, что произошло                                    
                    instance.PlayerControlMy.CmdResultSend(playerId, act, mes);
                break;
        }

        Debug.Log("Update something: player = " + playerId + " ; act = " + act.ToString() + " ; mes = " + mes);
    }

    private static bool CheckForEnd(bool checkForEnoughCards = false)
    {
        EndGameType end = EndGameType.None;
        MusicType mus = MusicType.None;

        bool myCardsEnough = checkForEnoughCards ? MyPlayer.HaveEnoughCardsForRound : true;
        bool eneCardsEnough = checkForEnoughCards ? EnemyPlayer.HaveEnoughCardsForRound : true;

        if ((MyPlayer.HP == 0 || !myCardsEnough) && (EnemyPlayer.HP == 0 || !eneCardsEnough))
        {
            end = EndGameType.Draw;
            mus = MusicType.Draw;
        }
        else if (MyPlayer.HP == 0 || !myCardsEnough)
        {
            end = EndGameType.Lose;
            mus = MusicType.Defeat;
        }
        else if (EnemyPlayer.HP == 0 || !eneCardsEnough)
        {
            end = EndGameType.Win;
            mus = MusicType.Victory;
        }

        if (end != EndGameType.None)
        {
            MenuManager.instance.UpdateGameEndImg((int)end);
            MenuManager.instance.VictoryLoseAnimControl.gameObject.SetActive(true);
            MenuManager.instance.VictoryLoseAnimControl.Show();
            Game.TimerKill();            
            OnPlayMusic(mus);
            return true;
        }

        return false;
    }

    public static void UpdatePlayersBuffsView()
    {
        MyPlayer.UpdateBuffsView();
        EnemyPlayer.UpdateBuffsView();
    }

    public static void ReturningAndDevidingPlayedCards()
    {
        for (int i = 0; i< 2; i++)
        {
            Pls[i].ReturnCardsFromDrop();
            Pls[i].DevidePlayedCards(Game.GetPlayedCardsAtLastRound(Pls[i].Id));
            Pls[i].UpdateDecksIcoTexts(isReactStage: false);
        }        
    }

    public static void ResetPlayersByRound(int round)
    {
        MyPlayer.ResetByRound(round);
        EnemyPlayer.ResetByRound(round);
    }

    public static void ResetPlayersByTurn()
    {
        MyPlayer.ResetAtTurn();
        EnemyPlayer.ResetAtTurn();
        MenuManager.instance.ClearHPChangesText();
    }

    public static void ResetPlayersReady(bool enableReadyBtn)
    {
        MyPlayer.SetIsReady(false);
        EnemyPlayer.SetIsReady(false);
        MenuManager.instance.SetPlayersNotReady(enableReadyBtn);
    }

    public static void ApplyPlayersBuffsDuration(DurationTypes dur)
    {
        MyPlayer.ChangeBuffsByApplingDuration(dur);
        EnemyPlayer.ChangeBuffsByApplingDuration(dur);            
    }

    public static void CalculateTurn()
    {
        Place myPlc = Game.GetPlace(MyId, Game.Turn - 1);
        Place ePlc = Game.GetPlace(EnemyId, Game.Turn - 1);

        Card myCard = myPlc.Card;
        Card eCard = ePlc.Card;

        string s = "\n";
        List<string> saList = new List<string>(); //сообщения аркмага о том, какие карты он уничтожил на этом ходу (нужен, чтоб не дублировать и выводить в конце хода)

        //список эффектов, которые должны быть отработать в этом ходу
        List<PlayedCardEffect> AllEffects = new List<PlayedCardEffect>();

        //добавляем эффекты с карт моих
        foreach (Effect ef in myCard.Effects)
            AllEffects.Add(new PlayedCardEffect(MyId, myPlc.Target, myCard.Id, ef, myPlc, MyCharacterId, myCard.Name));

        //добавляем эффекты с карт оппонента
        foreach (Effect ef in eCard.Effects)
            AllEffects.Add(new PlayedCardEffect(EnemyId, ePlc.Target, eCard.Id, ef, ePlc, EnemyPlayer.CharId, eCard.Name));

        //добавляем эффекты от бафов персонажей
        AddEffectFromBuffs(ref AllEffects);        

        //добавляем эффекты от пассивок персонажей - которые ПЕРЕД розыгрышем всех эффектов с карт и c бафов
        AddEffectsFromCharAbilitiesAtTurnBegin(ref AllEffects);        

        //сортировка эффектов c карт и с бафов по приоритету, затем по типу эффекта внутри одного приоритета, затем по имени эффекта (последнее только для того, чтоб одинаковый порядок у игроков соблюдался, были случаи рассинхрона)
        AllEffects = AllEffects.OrderBy(x => x.Effect.Priority).ThenBy(x => x.Effect.EffectType).ThenBy(x => x.Name).ToList();

        //запуск всех собранных и отсортированных эффектов в работу
        PlayedCardEffect pce;
        for (int i = 0; i < AllEffects.Count; i++)        
        {
            pce = AllEffects[i];
            if (pce.IsDisabled) continue; //пропуск отмененных эффектов

            Debug.Log(Pls[pce.PlayerId].Name + " " + pce.Name + " pr=" + pce.Effect.Priority + " ef=" + Lib.EffectNameDic[pce.Effect.EffectType]);
            
            bool ok = false; //действие карты успешно выполнилось (даже если бафы не смогли навешаться или дамаг не пройти - у них свои отдельные сообщения об успехе)

            s += Pls[pce.PlayerId].Name + " >> " + pce.Effect.Priority + "p " + pce.Name + ":  "; //инфа: чья и какая карта разыгрывается            

            if (CheckForGameEffectsPermission(pce)) //если действие карты разрешено
            {
                switch (pce.Effect.EffectType)
                {
                    case EffectTypes.Action: //на цели накинуть баф
                        foreach (Player p in GetTargetPlayers(pce))
                        {
                            ApplyBuffs(p, pce, ref s);
                        }
                        ok = true;
                        break;

                    case EffectTypes.CancelActions:
                        if (Game.AddEffect(pce.Effect))
                        {
                            s += " " + Lib.EffectNameDic[pce.Effect.EffectType] + "\n"; //выполнилось действие карты
                            ok = true;
                        }
                        break;

                    case EffectTypes.CancelAction:
                        if (Game.AddEffect(pce.Effect))
                        {
                            s += " " + Lib.EffectNameDic[pce.Effect.EffectType] + (pce.Effect.Points > 0 ? pce.Effect.Points + " и выше" : "любым") + "\n"; //выполнилось действие карты
                            ok = true;
                        }
                        break;

                    case EffectTypes.DamageUnblockable:
                    case EffectTypes.Damage:
                        foreach (Player p in GetTargetPlayers(pce))
                        {
                            ApplyDamage(p, pce, ref s);
                            ApplyBuffs(p, pce, ref s);
                        }
                        ok = true;
                        break;

                    case EffectTypes.Heal:
                        foreach (Player p in GetTargetPlayers(pce))
                        {
                            ApplyHeal(p, pce, ref s);
                            ApplyBuffs(p, pce, ref s);
                        }
                        ok = true;
                        break;

                    case EffectTypes.CancelBuffs:
                        foreach (Player p in GetTargetPlayers(pce))
                        {
                            CancelAllBuffs(p, ref s);                            

                            //найти в общем списке эффектов этого хода уже созданные эффекты из бафов этого чела и отменить их
                            foreach (PlayedCardEffect pceToCancel in AllEffects.Where(x => x.Place == null && x.CardId != -1 && x.PlayerId == p.Id).ToList())
                                pceToCancel.DisableEffect();                            

                            ApplyBuffs(p, pce, ref s);
                        }
                        ok = true;
                        break;

                    case EffectTypes.MirrorDamage:
                    case EffectTypes.MirrorActions:
                        s += " " + Lib.EffectNameDic[pce.Effect.EffectType] + "\n"; //выполнилось действие карты
                        foreach (Player p in GetTargetPlayers(pce))
                        {
                            ApplyBuffs(p, pce, ref s);
                        }
                        ok = true;
                        break;

                    case EffectTypes.SwapHP:
                        s += " " + Lib.EffectNameDic[pce.Effect.EffectType] + "\n"; //выполнилось действие карты
                        SwapPlayersHP(ref s);
                        ok = true;
                        break;

                    case EffectTypes.KillIfDamageDone:
                    case EffectTypes.Kill:
                        s += " " + Lib.EffectNameDic[pce.Effect.EffectType] + "\n"; //выполнилось действие карты
                        foreach (Player p in GetTargetPlayers(pce))
                        {
                            ok = KillPlayer(p, pce, ref s);
                        }
                        break;

                    case EffectTypes.DropToGrave:
                        s += " " + Lib.EffectNameDic[pce.Effect.EffectType] + "\n"; //выполнилось действие карты
                        foreach (Player p in GetTargetPlayers(pce))
                        {
                            ok = ApplyDropToGrave(p, pce, ref s);
                        }
                        break;
                }
            }
            else 
            {
                //Archmage - Когда отменяет заклинание, которое должно было нанести урон - оно уходит в сброс навсегда.
                if ((pce.PlayerId == MyId && EnemyPlayer.CharId == 1) || (pce.PlayerId == EnemyId && MyPlayer.CharId == 1)) 
                {
                    if ((pce.Effect.EffectType == EffectTypes.Damage || pce.Effect.EffectType == EffectTypes.DamageUnblockable) && pce.Place != null && !pce.Place.IsCopy && pce.CardId != -1) //если эффект - дамага, от карты - не копии и не от бафа и не от пассивки
                    {
                        pce.Place.SetSpecialWayAfterRound(WayForCard.ToGrave); //пометить место с картой на отправку в могилу (когда распределение карт будет в конце раунда)
                        string sa = OtherPlayer(Pls[pce.PlayerId]).Name + " >> *Cпособность Archmage*: карта " + Lib.CardsDic[pce.CardId].Name + " игрока " + Pls[pce.PlayerId].Name + " больше не вернется в колоду!\n";

                        if (!saList.Contains(sa))
                            saList.Add(sa);                        
                    }
                }
            }
            
            if (!ok)
                s += " " + Lib.EffectNameDic[pce.Effect.EffectType] + " - неудача!\n";

            //проверка на конец игры происходит после выполнения всех действий в одном приоритете            
            if (i >= AllEffects.Count - 1 || pce.Effect.Priority != AllEffects[i + 1].Effect.Priority)
            { 
                //есть ли проигравшие -> конец игры                    
                if (CheckForEnd())
                    break;
            }
        }

        //отдельный запуск пассивок персонажей - которые должны отработать ПОСЛЕ розыгрыша всех эффектов (эти пассивки не трансформируются в эффекты) //пока тут только пассивка Demonic - лечит себя
        ApplyCharacterAbilitiesAtTurnEnd(ref s);

        if (saList.Count > 0) //сообщение об уничтоженных картах аркмагом
            foreach (string ss in saList)
                s += ss;

        //вывести сообщение с результатами хода на экран        
        MenuManager.instance.UpdateTurnInfo(s);
    }

    //проверка на блокировку этого действия уже разыгранными ранее эффектами с карт в этом ходу
    private static bool CheckForGameEffectsPermission(PlayedCardEffect pce)
    {
        //если ранее была разыграна Avendos (EffectTypes.CancelActions), то не работают никакие карты, кроме Avendos (чтоб враг тоже смог повесить на себя баф этой карты)
        if (Game.CheckForEffect(EffectTypes.CancelActions) && pce.CardId != 1) return false;

        bool ok = true;

        //если ранее была разыграна карта с отменой действия ( в тч по приоритету) //Sinx, Wizardo
        if (Game.CheckForEffect(EffectTypes.CancelAction))
        {
            Effect ef = Game.GetEffect(EffectTypes.CancelAction);

            if (ef.Points == 0)
                ok = false;
            else
            {
                if (pce.Effect.Priority >= ef.Points)
                {
                    ok = false;

                    //здесь по-хорошему должна быть проверка на карту Sinx, но я в эффекте не храню карту-источник - единственное место, где оно понадобилось
                    if (pce.Place != null)
                        pce.Place.SetSpecialWayAfterRound(WayForCard.ToDeck); //пометить место с картой на возврат в колоду (когда распределение карт будет в конце раунда)
                }
            }            
        }

        return ok;
    }

    private static void AddEffectFromBuffs(ref List<PlayedCardEffect> AllEffects)
    {        
        for (int i = 0; i < 2; i++)
        {
            List<PlayedCardEffect> pcesFromBuffs = Pls[i].GetPlayedCardEffectsFromBuffs(i, AllEffects);

            if (pcesFromBuffs.Count > 0)            
                AllEffects.AddRange(pcesFromBuffs);
        }
    }

    private static void AddEffectsFromCharAbilitiesAtTurnBegin(ref List<PlayedCardEffect> AllEffects)
    {   
        foreach (Player p in Pls)
        {
            if (p.CharId == 3) //Necromancer 
            {
                //Когда здоровье понижается до 1-ого, противник получает 4 урона каждый ход
                if (p.HP <= 1 || p.CheckForBuff(BuffTypes.ActivateClassAbilities)) //у некра еще бафом активируется эта пассивка
                    AllEffects.Add(new PlayedCardEffect(playerId: p.Id, target: Target.Enemy, cardId: -1, effect: new Effect(priority: 7, effectType: EffectTypes.Damage, target: Target.Enemy, points: 4, buffs: null), place: null, charId: p.CharId, name: "*Способность Necromancer (HP <= 1)*"));                
            }
        }    
    }

    private static void AddBuffsFromCharAbilitiesAtTurnEnd()
    {
        foreach (Player p in Pls)
        {
            switch (p.CharId)
            {
                case 3: //Necromancer             
                //Когда здоровье понижается до 5-ти и ниже - наносит дополнительно 2 урона заклинаниями, наносящими урон (урон от пассивок не в счет)
                if (p.HP <= 5) //+баф с +2 к дамагу
                {
                    Buff b = new Buff(sourceCardId: -1, buffType: BuffTypes.IncDamage, duration: new Duration() { DurType = DurationTypes.TurnsLeft, Turns = 1 }, points: 2, canStack: false, isContinuous: true, isOnTurnsView: false, isClassAbility: true);
                    p.AddBuff(b, p.Id, p.CharId, Target.None, updateView: false);
                }
                break;                

                case 5: //Vampire             
                //Если у Vampire меньше здоровья, чем у соперника, Vampire наносит двойной урон картами редкости Common.
                if (p.HP < OtherPlayer(p).HP) //+баф с x2 к дамагу
                {
                    Buff b = new Buff(sourceCardId: -1, buffType: BuffTypes.MultiplyCommonDamage, duration: new Duration() { DurType = DurationTypes.TurnsLeft, Turns = 1 }, points: 2, canStack: false, isContinuous: true, isOnTurnsView: false, isClassAbility: true);
                    p.AddBuff(b, p.Id, p.CharId, Target.None, updateView: false);
                }
                break;            
            }
        }
    }

    private static void ApplyCharacterAbilitiesAtTurnEnd(ref string s)
    {        
        List<Player> pls = new List<Player>();

        pls.AddRange(Pls.Where(x => x.CharId == 4)); //ищем демоников        

        foreach(Player p in pls)
        {            
            if (p.CharId == 4) //Demonic - Восстанавливает 2 пункта здоровья в ход в случае, если не получает урон
            {
                if (p.SumDamageAtTurn == 0)
                {
                    if (p.AddHeal(2) > 0)
                    {
                        s += p.Name + " >> *Cпособность Demonic* (не получал урон): " + p.Name + " +2HP\n";
                        MenuManager.instance.UpdatePlayerHPText(p.IsEnemy ? 1 : 0, p.HP, p.SumDamageAtTurn, p.SumHealAtTurn);
                        OnPlaySFX(SFXType.Heal);
                    }
                    else
                        s += p.Name + " >> *Cпособность Demonic* (не получал урон): " + p.Name + " +2HP - Неудача!\n";
                }
            }
        }
    }

    private static bool ApplyDropToGrave(Player p, PlayedCardEffect pce, ref string s)
    {
        if (p == null || pce == null) return false;

        if (p.CheckForBuff(BuffTypes.MirrorActions) && !OtherPlayer(p).CheckForBuff(BuffTypes.MirrorActions)) //если на целевом игроке висит отражени, а на его оппоненте - нет, то эффект улетает в оппонента
            p = OtherPlayer(p);

        string diedCards = "";

        if (p.DropToGrave(ref diedCards))
        {
            p.UpdateDecksIcoTexts(isReactStage: false, virtualHandDec: Game.GetCopiesCountAtCurrentRound(p.IsEnemy ? EnemyId : MyId, untilCurrentTurn: true));
            s += p.Name + " - сброс уничтожен! (" + diedCards + ")\n";
        }
        else
            s += p.Name + " - сброс пустой!\n";

        return true;
    }

    private static bool KillPlayer(Player p, PlayedCardEffect pce, ref string s)
    {
        if (p == null || pce == null) return false;

        if (p.CheckForBuff(BuffTypes.MirrorActions) && !OtherPlayer(p).CheckForBuff(BuffTypes.MirrorActions)) //если на целевом игроке висит отражени, а на его оппоненте - нет, то смерть улетает в оппонента
            p = OtherPlayer(p);

        if (p.CheckForBuff(BuffTypes.CantDie))
            return false; //если на целевом игроке (или в том, в кого отразилось) висит защита от смерти, то игрок не умирает

        if (pce.Effect.EffectType == EffectTypes.KillIfDamageDone && Pls[pce.PlayerId].DamageDealt < pce.Effect.Points)
            return false; //если не накопил необходимого "нанесенного дамага оппоненту", то не работает //если накопил, и эту карту отразят обратно, -  убьёт сам себя (даже если враг столько не накопил)

        p.SetNewHeal(0, p.SumDamageAtTurn, p.SumHealAtTurn);
        s += p.Name + " - Смерть!\n";

        MenuManager.instance.UpdatePlayerHPText(p.IsEnemy ? 1 : 0, p.HP, p.SumDamageAtTurn, p.SumHealAtTurn);

        return true;
    }


    private static void SwapPlayersHP(ref string s)
    {   
        int TempHP = MyPlayer.HP;
        int TempSumDamageAtTurn = MyPlayer.SumDamageAtTurn;
        int TempSumHealAtTurn = MyPlayer.SumHealAtTurn;        

        MyPlayer.SetNewHeal(EnemyPlayer.HP, EnemyPlayer.SumDamageAtTurn, EnemyPlayer.SumHealAtTurn);
        s += MyPlayer.Name + ": " + TempHP + "HP => " + MyPlayer.HP + "HP ; " + EnemyPlayer.Name + ": " + EnemyPlayer.HP + "HP => ";

        EnemyPlayer.SetNewHeal(TempHP, TempSumDamageAtTurn, TempSumHealAtTurn);
        s += EnemyPlayer.HP + "HP\n";

        MenuManager.instance.UpdatePlayerHPText(0, MyPlayer.HP, MyPlayer.SumDamageAtTurn, MyPlayer.SumHealAtTurn);
        MenuManager.instance.UpdatePlayerHPText(1, EnemyPlayer.HP, EnemyPlayer.SumDamageAtTurn, EnemyPlayer.SumHealAtTurn);
    }

    private static void CancelAllBuffs(Player p, ref string s)
    {
        if (p == null) return;

        if (p.CheckForBuff(BuffTypes.MirrorActions) && !OtherPlayer(p).CheckForBuff(BuffTypes.MirrorActions)) //если на целевом игроке висит отражение, а на его оппоненте - нет, то сброс бафов улетает в оппонента
            p = OtherPlayer(p);

        s += p.Name + " сброс всех продолжительных заклинаний\n";

        p.CancelAllBuffs();            
    }

    private static void ApplyHeal(Player p, PlayedCardEffect pce, ref string s)
    {
        if (p == null || pce == null || pce.Effect.Points <= 0) return;        

        if (pce.Effect.SourcePlayerId != -1 && Pls[pce.Effect.SourcePlayerId].CheckForBuff(BuffTypes.HealToDamage)) //если на игроке-источнике этого лечения висит баф "вместо хила - урон", то перенаправляем это все в дамаг
        {
            ApplyDamage(p, pce, ref s);
            return;
        }

        if (p.CheckForBuff(BuffTypes.MirrorActions) && !OtherPlayer(p).CheckForBuff(BuffTypes.MirrorActions)) //если на целевом игроке висит отражение, а на его оппоненте - нет, то хилка улетает в оппонента
            p = OtherPlayer(p);

        int healResult = p.AddHeal(pce.Effect.Points);

        if (healResult > 0)
        {            
            s += p.Name + " +" + healResult + "HP\n";
            MenuManager.instance.UpdatePlayerHPText(p.IsEnemy ? 1 : 0, p.HP, p.SumDamageAtTurn, p.SumHealAtTurn);
            OnPlaySFX(SFXType.Heal);
        }
        else
        {
            s += p.Name + " +" + pce.Effect.Points + "HP - неудача!\n";
        }
    }

    private static void ApplyDamage(Player p, PlayedCardEffect pce, ref string s)
    {
        if (p == null || pce == null || pce.Effect.Points <= 0) return;

        bool isMirrored = false;

        if ((p.CheckForBuff(BuffTypes.MirrorActions) || p.CheckForBuff(BuffTypes.MirrorDamage)) && (!OtherPlayer(p).CheckForBuff(BuffTypes.MirrorActions) && !OtherPlayer(p).CheckForBuff(BuffTypes.MirrorDamage))) //если на целевом игроке висит "отражение всего" либо "отражение дамага", а на его оппоненте - нет, то дамаг улетает в оппонента
        {
            p = OtherPlayer(p);
            isMirrored = true;
        }

        //Определение игрока - источника дамага
        Player DamagePlayer = Pls[pce.PlayerId]; //игрок, считающийся источником дамага

        if (DamagePlayer == p && isMirrored) //если сам себя хочет бить и урон был отраженный, то "источник дамага" тот, кто отразил
            DamagePlayer = OtherPlayer(p);

        if (pce.Effect.SourcePlayerId > -1) //если дамаг прилетел из ранее навешанного кем-то бафа, то "исчточник дамага" тот, кто баф навешивал/отражал
            DamagePlayer = MyId == pce.Effect.SourcePlayerId ? MyPlayer : EnemyPlayer;
        

        int comingDamage = pce.Effect.Points; //изначальный дамаг, который летит в цель

        if (DamagePlayer.CheckForBuff(BuffTypes.MultiplyCommonDamage) && pce.CardId > 0 && Lib.CardsDic[pce.CardId].Rarity == CardRarityes.Common)
        {
            comingDamage *= 2;
            s += "*Cпособность Vampire* ";
        }

        if (DamagePlayer.CheckForBuff(BuffTypes.IncDamage) && pce.CardId != -1)
        {
            comingDamage += 2;
            s += "*Cпособность Necromancer* ";
        }        

        int damageResult = p.AddDamage(damage: comingDamage, unblockable: pce.Effect.EffectType == EffectTypes.DamageUnblockable, priority: pce.Effect.Priority);

        if (damageResult > 0)
        {
            if (DamagePlayer != p) //если сам себя бил просто так - не считается "дамагом по оппоненту"            
                DamagePlayer.AddDamageDealt(damageResult); //атакующий (источник дамага) запоминает, сколько урона нанес за игру (только если бил оппонента)

            s += p.Name + " -" + damageResult + "HP\n";
            MenuManager.instance.UpdatePlayerHPText(p.IsEnemy ? 1 : 0, p.HP, p.SumDamageAtTurn, p.SumHealAtTurn);
            OnPlaySFX(SFXType.Damage);
        }
        else
        {
            s += p.Name + " -" + comingDamage + "HP - неудача!\n";
        }
    }

    private static void ApplyBuffs(Player p, PlayedCardEffect pce, ref string s)
    {
        if (p == null || pce == null || pce.Effect.Buffs == null) return;

        bool isMirrored = false;

        if (p.CheckForBuff(BuffTypes.MirrorActions) && !OtherPlayer(p).CheckForBuff(BuffTypes.MirrorActions)) //если на целевом игроке висит отражение, а не его оппоненте - нет, то баффы улетают в оппонента
        {
            p = OtherPlayer(p);
            isMirrored = true;
        }

        //если цель для бафа в самом бафе не задана, то либо она должна выбираться игроком и берется из места карты, где игрок задал цель, либо берется прямо из эффекта
        Target tg = pce.Effect.Target != Target.Select ? pce.Effect.Target : pce.Target;

        int sourcePlayerId = pce.PlayerId;

        if (isMirrored) //если баф был отражен, источником бафа становится тот, кто отразил
            sourcePlayerId = sourcePlayerId == 0 ? 1 : 0;        

        foreach (Buff b in pce.Effect.Buffs)
        {            
            if (p.AddBuff(b, sourcePlayerId, Pls[sourcePlayerId].CharId, tg)) //если цель для бафа уже задана в самом бафе, то она сохранится            
                s += p.Name + " +" + Lib.BuffNameDic[b.BuffType] + b.DurationInfo + "\n";
            else            
                s += p.Name + " +" + Lib.BuffNameDic[b.BuffType] + " - неудача!\n";            
        }
    }

    private static List<Player> GetTargetPlayers(PlayedCardEffect pce)
    {
        if (pce == null) throw new System.ArgumentException("GameManager.GetTargetPlayers(pce): pce = null");

        //если цель должна выбираться игроком, то берется из места карты, где игрок задал цель; иначе - берется прямо из эффекта
        Target tg = pce.Effect.Target != Target.Select ? pce.Effect.Target : pce.Target; 

        List<Player> targetPls = new List<Player>();
        if (tg == Target.Me || tg == Target.All) targetPls.Add(Pls[pce.PlayerId]);
        if (tg == Target.Enemy || tg == Target.All) targetPls.Add(OtherPlayer(Pls[pce.PlayerId]));
        return targetPls;
    }    

    private static bool CheckForDeckReady(int player)
    {
        bool ok = true;
        Place pl = null;        

        for (int i = 0; i < 3; i++)
        {
            pl = Game.GetPlace(player, i);

            if (pl.Card == null || pl.Card.Id == 0 || (pl.Card.Effects.Any(x => x.Target == Target.Select) && pl.Target != Target.Enemy && pl.Target != Target.Me))
            {
                ok = false;
                break;
            }
        }

        return ok;
    }

    //Когда удаляет либо заменяет карту со стола, нужно проверять все карты справа от неё на её дубликаты и удалять их со стола тоже
    private static void DeleteDuplicatedCardsFromPlace(int placeFrom, int player, Player p, bool IsMyAction)
    {
        //проверка на копии соседних карт справа
        if (placeFrom < 3)
        {
            int to = 3 - placeFrom;
            if (to > 0)
            {
                for (int i = placeFrom; i < placeFrom + to; i++)
                {
                    Place plNext = Game.GetPlace(player, i);

                    if (plNext.Card != null && plNext.IsCopy)
                    {
                        plNext.SetCard(null);
                        p.UseTokens(decrease: false, round: Game.Round);

                        if (MyPlayer.RoundSee == Game.Round) //показывать движение карт актуального раунда игроку только тогда, когда у него открыт этот раунд на столе
                            MenuManager.instance.DeskControl.RemoveCardFromPlace(IsMyAction, i + 1);
                    }
                    else break; //дальше одной НЕкопии не смотрим
                }
            }
        }
    }

    public static void ResetGame(bool closeConnection = true)
    {
        if (closeConnection)
        {
            ResetMeHost();
            State = GameStates.Off;

            MyPlayer.OnGatherFull -= MenuManager.instance.ReadyBtnEnable; //отписка кнопки "Готов"

            //отписка от всего остального
            MyPlayer.UnsubscribeOnEvents();
            EnemyPlayer?.UnsubscribeOnEvents();

            Pls[0]?.Reset();
            Pls[1]?.Reset();

            OnPlayMusic(MusicType.MainMenu);
        }
        else
            State = GameStates.SelectChar;        
        
        Game?.Dispose();
        Game = null;
    }

    public void MyDeckReady()
    {
        List<CardInfo> cis = MenuManager.instance.HandControl.GetCards();
        List<int> ids = new List<int>();

        foreach (CardInfo ci in cis)           
            ids.Add(ci.Id);        

        PlayerControlMy.MyAction(Actions.MyDeck, string.Join(",", ids));
    }

    public void ClickHandCard(int index)
    {
        if (Game == null || (Game.Stage != GameStages.Put && Game.Stage != GameStages.React) || MyPlayer.IsReady || !MyGatherReady) return;

        if (MyPlayer.SelectedCard > -1)
            MenuManager.instance.HandControl.DeselectCard(MyPlayer.SelectedCard);

        if (MyPlayer.SelectedCard != index)
        {
            MenuManager.instance.HandControl.SelectCard(index);

            //зеленая подсветка - только в текущий раунд на столе можно карты ложить            
            if (MyPlayer.RoundSee == Game.Round)
                MenuManager.instance.DeskControl.EnablePlaces();

            MyPlayer.SetSelectedCard(index);
        }
        else
        {
            MenuManager.instance.DeskControl.DisablePlaces();
            MyPlayer.SetSelectedCard(-1);
        }
    }

    public void ClickDeskCard(int index, int row)
    {
        //здесь index = [0,4] (5 мест для карт на столе, из них [1,3] - карты текущего раунда), а в Game.Desk index = [0,2] (3 карты текущего раунда)        

        if (row != 0 || index < 1 || index > 3 || MyPlayer.IsReady || MyPlayer.RoundSee != Game.Round || !MyGatherReady) return; //игрок может кликать только по своим местам для карт на столе

        switch (Game.Stage)
        {
            case GameStages.Put:                                
                PlayerControlMy.MyAction(Actions.PutCardToDesk, MyPlayer.SelectedCard + "," + index);
                break;

            case GameStages.React:                
                if (index <= Game.Turn) break; //только карты еще не разыгранных ходов можно менять
                if (index == 2 && Game.GetPlace(MyId, 2).IsCopy) break; //нельзя источник копии менять в реакт стадии    
                
                //если уже убирал карту со стола в реакт стадии, то может только вернуть её назад, либо положить в это место другую карту
                if (MyPlayer.LastReact != null && (MyPlayer.LastReact.IsDone || index - 1 != MyPlayer.LastReact.IndexFrom || MyPlayer.LastReact.AtRound != Game.Round)) break; //можно трогать только то место, откуда ранее карту убрал и только на том же ходу

                PlayerControlMy.MyAction(Actions.PutCardToDesk, MyPlayer.SelectedCard + "," + index);                
                break;
        }
    }

    public void ClickArrow(int placeId, int arrowId)
    {
        if ((Game.Stage != GameStages.Put && Game.Stage != GameStages.React) || placeId < 1 || placeId > 3 || arrowId < 0 || arrowId > 1 && MyPlayer.RoundSee != Game.Round || MyPlayer.IsReady) return;
        if (Game.Stage == GameStages.React && placeId <= Game.Turn) return; //только стрелки у карт еще не разыгранных ходов можно менять

        Place place = Game.GetPlace(MyId, placeId - 1);
        if (place.Card == null || !place.Card.Effects.Any(x => x.Target == Target.Select)) return;

        PlayerControlMy.MyAction(Actions.SetCardTarget, placeId + "," + arrowId);        
    }

    public void ClickToken(int placeId)
    {
        if ((Game.Stage != GameStages.Put && Game.Stage != GameStages.React) || placeId < 2 || placeId > 3 || MyPlayer.RoundSee != Game.Round || !MyPlayer.HasTokens || MyPlayer.IsReady) return;

        Place place = Game.GetPlace(MyId, placeId - 1);
        Place placeSource = Game.GetPlace(MyId, placeId - 2);

        if (place.Card != null || placeSource.Card == null || !placeSource.Card.CanBeCopied(MyCharacterId)) return;

        PlayerControlMy.MyAction(Actions.CopyCard, placeId.ToString());
    }

    public void ClickReady()
    {
        if (Game.Stage != GameStages.Put && Game.Stage != GameStages.React) return;        

        if (MyGatherReady)
        {
            if (!MyPlayer.IsReady && MyPlayer.RoundSee == Game.Round && MenuManager.instance.DeskControl.CheckForFullDesk())
                PlayerControlMy.MyAction(Actions.ImReady, "");
        }
        else
        {
            if (MyPlayer.GatherCards == 0)            
                PlayerControlMy.MyAction(Actions.SetGatherCards, string.Join(",", MyPlayer.SelectedDeckCards));
        }
    }

    public void ScrollDesk(bool left)
    {
        if (Game.Stage == GameStages.Turn) return; //нельзя двигать стол во время визуализации ходов

        int wantedRound = left ? MyPlayer.RoundSee - 1 : MyPlayer.RoundSee + 1;

        var map = Game.GetDeskMap(MyId, wantedRound);

        if (map != null)
        {
            MyPlayer.SetRoundSee(wantedRound);
            MenuManager.instance.DeskControl.MoveDesk(map, showEarly: wantedRound > 0, roundIsCurrent: wantedRound == Game.Round, roundIsPrevious: wantedRound == Game.Round - 1, selectedCard: MyPlayer.SelectedCard, currentTurn: Game.Turn, isPutStage: Game.Stage == GameStages.Put, ImNotReady: !MyPlayer.IsReady, myGatherReady: MyGatherReady);
            OnPlaySFX(SFXType.MoveTable);
        }
    }

    public void ClickDeckCard(int index)
    {
        if (Game.Stage != GameStages.Put || MyPlayer.IsReady || MyGatherReady) return;

        if (MyPlayer.SelectedDeckCards.Contains(index) && MyPlayer.RemoveSelectedDeckCard(index))
        {            
            MenuManager.instance.DeckControl.DeselectCard(index);
            OnPlaySFX(SFXType.CardPlacement);
        }
        else if (MyPlayer.AddSelectedDeckCard(index))
        {
            MenuManager.instance.DeckControl.SelectCard(index);
            OnPlaySFX(SFXType.CardPlacement);
        }       
    }    

    public void CardsSelectUpdateClick()
    {
        if (MyGatherReady) return;

        //если была выделена карта - задвинуть карты в руке + сбросить выделение
        if (MyPlayer.SelectedCard > -1)
        {
            MenuManager.instance.HandControl.DeselectCard(MyPlayer.SelectedCard);
            MenuManager.instance.DeskControl.DisablePlaces();
            MyPlayer.SetSelectedCard(-1);
        }        

        MyPlayer.DeskSeeChange();                
        MenuManager.instance.UpdateDeck(show: !MyDeskSee);
        MenuManager.instance.DeskShowHideBtnControl.UpdateBtn();
    }

    public void NextGame()
    {        
        ResetGame(closeConnection: false);
        MenuManager.instance.UpdateShowedCharsData();
        UpdatePlayersBuffsView();
        MenuManager.instance.ResetAllOnEnd();
        MenuManager.instance.HideGameDesk();

        MenuManager.instance.SelectCharPanelAnimControl.gameObject.SetActive(true);
        MenuManager.instance.SelectCharPanelAnimControl.Show();
    }    

    //таймеры продолжали тикать, когда игру в редакторе останавливал
    private void OnDestroy()
    {
        ResetGame(); 
    }

    public void NextTurnStep(object sender, StepEventArgs e)
    {        
        //тики таймера должны выполняться в MainThread, иначе валятся ошибки: get_isActiveAndEnabled can only be called from the main thread.
        //поэтому степ из таймера помещается в переменную TurnStepToUpdate и в Update по ней позже запускается MyAction

        TurnStepToUpdate = e.Step;
    }

    private void Update()
    {
        //проверка на отложенный тик таймера, если не в Update тики обрабатывать - в юнити ошибки валятся
        if (TurnStepToUpdate > 0)
        {            
            PlayerControlMy.MyAction(Actions.NextTurnStep, TurnStepToUpdate.ToString());
            TurnStepToUpdate = 0;
        }
    }   
}
