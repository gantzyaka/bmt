﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using GameTypes;


public class CardInfo : InfoEntity {

    public Button Btn;


    public void SetCard(int cardId)
    {
        if (cardId < 1 || Id > Lib.CardsDic.Count) return;

        Img.sprite = Lib.instance.CardsImgs[cardId];
        Id = cardId;
    }

    public void CopyCard(CardInfo ci)
    {
        if (ci == null) return;

        Img.sprite = ci.Img.sprite;
        Id = ci.Id;
    }    

    public void SetOnClick(CardInfoClicks action, int index =-1, int row = -1)
    {
        switch (action)
        {
            case CardInfoClicks.RemoveFromHand:
                Btn.onClick.RemoveAllListeners();
                Btn.onClick.AddListener(() => MenuManager.instance.HandControl.RemoveCard(this));
                Btn.onClick.AddListener(() => SoundManager.instance.CardClickSound()); SoundManager.instance.CardClickSound();
                break;

            case CardInfoClicks.ClickHandCard:
                Btn.onClick.RemoveAllListeners();
                Btn.onClick.AddListener(() => GameManager.instance.ClickHandCard(index));
                break;

            case CardInfoClicks.ClickDeskCard:
                Btn.onClick.RemoveAllListeners();
                Btn.onClick.AddListener(() => GameManager.instance.ClickDeskCard(index, row));
                break;            

            case CardInfoClicks.ClickDeckCard:
                Btn.onClick.RemoveAllListeners();
                Btn.onClick.AddListener(() => GameManager.instance.ClickDeckCard(index));                
                break;
        }
    }
}
