﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameTypes;
using System.Linq;
using System;

public class Lib : MonoBehaviour {
    
    public static Lib instance { get; private set; }    

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);


        //TODO Загрузка всей инфы из файла (карты, персонажи)
        //пока захардкожено
        CharsDic = new Dictionary<int, Character>()
        {
            { 1, new Character(id: 1, hP: 20, name: "Archmage", discription: "Когда отменяет заклинание, которое должно было нанести урон, оно уходит в сброс навсегда.\nМожет копировать карты редкости Ultimate.") },
            { 2, new Character(id: 2, hP: 18, name: "Blastmage", discription: "Способности, наносящие урон, выполняются на втором приоритете.\nИмеет два дополнительных копирования.") },
            { 3, new Character(id: 3, hP: 15, name: "Necromancer", discription: "Когда здоровье понижается до 5 и ниже, наносит дополнительно 2 урона заклинаниями, наносящими урон.\nКогда здоровье понижается до 1, противник получает 4 урона каждый ход.") },
            { 4, new Character(id: 4, hP: 13, name: "Demonic", discription: "Не получает больше 3 единиц урона в течение одного приоритета.\nВосстанавливает 2 единицы здоровья на каждом ходу, в котором не получил урона.") },
            { 5, new Character(id: 5, hP: 10, name: "Vampire", discription: "Не имеет предела здоровья.\nНаносит двойной урон картами редкости Common, если имеет меньше здоровья, чем соперник.", hasLimitHP: false ) },
        };


        AnimDic = new Dictionary<AnimTypes, AnimationClip>
        {
            { AnimTypes.FadeInImage, Anims[0] },
            { AnimTypes.FadeOutImage, Anims[1] },
            { AnimTypes.FadeInText, Anims[2] },
            { AnimTypes.FadeOutText, Anims[3] },
        };

        CardsDic = new Dictionary<int, Card>()
        {
            {1, new Card(id: 1, name: "Avendos", discription: "", rarity: CardRarityes.Ultimate, type: CardTypes.Ancient, returnType: CardReturnToDeckTypes.Never,
                effects: new List<Effect>()
                {
                    new Effect(priority: 0, effectType: EffectTypes.CancelActions, target: Target.All, points: 0, buffs: null),
                    new Effect(priority: 0, effectType: EffectTypes.Action, target: Target.Me, points: 0, buffs: new List<Buff>() {
                            new Buff(sourceCardId: 1, buffType: BuffTypes.NoDamage, duration: new Duration() { DurType = DurationTypes.EndRound }, priority: 0, isContinuous: true)
                        } ),
                } )
            },
            {2, new Card(id: 2, name: "Omnicaidance", discription: "", rarity: CardRarityes.Ultimate, type: CardTypes.Ancient, returnType: CardReturnToDeckTypes.Never,
                effects: new List<Effect>()
                {
                    new Effect(priority: 1, effectType: EffectTypes.CancelBuffs, target: Target.Select, points: 0, buffs: new List<Buff>() {
                            new Buff(sourceCardId: 2, buffType: BuffTypes.NoBuffs, duration: new Duration() { DurType = DurationTypes.EndRound }, priority: 1)
                        } ),
                } )
            },
            {3, new Card(id: 3, name: "Laniakreya", discription: "", rarity: CardRarityes.Ultimate, type: CardTypes.Ancient, returnType: CardReturnToDeckTypes.Never,
                effects: new List<Effect>()
                {
                    new Effect(priority: 3, effectType: EffectTypes.Heal, target: Target.Select, points: 10, buffs: null)
                } )
            },
            {4, new Card(id: 4, name: "Indario", discription: "", rarity: CardRarityes.Ultimate, type: CardTypes.Ancient, returnType: CardReturnToDeckTypes.Never,
                effects: new List<Effect>()
                {
                    new Effect(priority: 1, effectType: EffectTypes.MirrorActions, target: Target.Me, points: 0, buffs: new List<Buff>() {
                            new Buff(sourceCardId: 4, buffType: BuffTypes.MirrorActions, duration: new Duration() { DurType = DurationTypes.TurnsLeft, Turns = 1 }, priority: 1)
                        } ),
                } )
            },
            {5, new Card(id: 5, name: "Ontares", discription: "", rarity: CardRarityes.Ultimate, type: CardTypes.Ancient, returnType: CardReturnToDeckTypes.Never,
                effects: new List<Effect>()
                {
                    new Effect(priority: 1, effectType: EffectTypes.Action, target: Target.Me, points: 0, buffs: new List<Buff>() {
                            new Buff(sourceCardId: 5, buffType: BuffTypes.CantDie, duration: new Duration() { DurType = DurationTypes.EndRound }, priority: 1, isContinuous: true)
                        } ),
                } )
            },
            {6, new Card(id: 6, name: "Ertheanos", discription: "", rarity: CardRarityes.Ultimate, type: CardTypes.Ancient, returnType: CardReturnToDeckTypes.Never,
                effects: new List<Effect>()
                {
                    new Effect(priority: 4, effectType: EffectTypes.Damage, target: Target.Enemy, points: 5, buffs: null),
                    new Effect(priority: 4, effectType: EffectTypes.Damage, target: Target.Me, points: 4, buffs: null)
                } )
            },
            {7, new Card(id: 7, name: "Salkartres", discription: "", rarity: CardRarityes.Common, type: CardTypes.Ancient, returnType: CardReturnToDeckTypes.AtNextRound,
                effects: new List<Effect>()
                {
                    new Effect(priority: 4, effectType: EffectTypes.Damage, target: Target.Me, points: 1, buffs: new List<Buff>() {
                            new Buff(sourceCardId: 7, buffType: BuffTypes.AddDamage, duration: new Duration() { DurType = DurationTypes.EndingCondition, EndingCondition = EffectTypes.Damage, EndingConditionTarget = Target.Enemy }, points: 3, priority: 5, isContinuous: true, activationTime: ActivationTimeTypes.AtEnd, target: Target.Enemy, isOnTurnsView: false)
                        } ),

                } )
            },
            {8, new Card(id: 8, name: "Gallatrix", discription: "", rarity: CardRarityes.Common, type: CardTypes.Ancient, returnType: CardReturnToDeckTypes.AtNextRound,
                effects: new List<Effect>()
                {
                    new Effect(priority: 4, effectType: EffectTypes.Damage, target: Target.Select, points: 3, buffs: null),
                } )
            },
            {9, new Card(id: 9, name: "Gratas", discription: "", rarity: CardRarityes.Common, type: CardTypes.Ancient, returnType: CardReturnToDeckTypes.AtNextRound,
                effects: new List<Effect>()
                {
                    new Effect(priority: 1, effectType: EffectTypes.Action, target: Target.Me, points: 0, buffs: new List<Buff>() {
                            new Buff(sourceCardId: 9, buffType: BuffTypes.NoDamage, duration: new Duration() { DurType = DurationTypes.TurnsLeft, Turns = 1 }, priority: 1)
                        } ),
                } )
            },
            {10, new Card(id: 10, name: "Sensenia", discription: "", rarity: CardRarityes.Common, type: CardTypes.Ancient, returnType: CardReturnToDeckTypes.AtNextRound,
                effects: new List<Effect>()
                {
                    new Effect(priority: 3, effectType: EffectTypes.Heal, target: Target.Select, points: 3, buffs: null)
                } )
            },
            {11, new Card(id: 11, name: "Sinx", discription: "", rarity: CardRarityes.Common, type: CardTypes.Ancient, isBlockedForCopying : true, returnType: CardReturnToDeckTypes.AtNextRound,
                effects: new List<Effect>()
                {
                    new Effect(priority: 1, effectType: EffectTypes.CancelAction, target: Target.All, points: 4, buffs: null) //4 = отменяет заклинаний приоритета 4 и выше
                } )
            },
            {12, new Card(id: 12, name: "Nairea", discription: "", rarity: CardRarityes.Common, type: CardTypes.Ancient, returnType: CardReturnToDeckTypes.AtNextRound,
                effects: new List<Effect>()
                {
                    new Effect(priority: 1, effectType: EffectTypes.CancelBuffs, target: Target.Select, points: 0, buffs: null)
                } )
            },
            {13, new Card(id: 13, name: "Wizardo", discription: "", rarity: CardRarityes.Common, type: CardTypes.C_Archmage, returnType: CardReturnToDeckTypes.AtNextRound,
                effects: new List<Effect>()
                {
                    new Effect(priority: 1, effectType: EffectTypes.CancelAction, target: Target.All, points: 0, buffs: null) //0 = любого приортета отменяет карту
                } )
            },
            {14, new Card(id: 14, name: "Argonyte", discription: "", rarity: CardRarityes.Common, type: CardTypes.C_Archmage, returnType: CardReturnToDeckTypes.AtNextRound,
                effects: new List<Effect>()
                {
                    new Effect(priority: 4, effectType: EffectTypes.Damage, target: Target.Enemy, points: 2, buffs: null),
                    new Effect(priority: 4, effectType: EffectTypes.Action, target: Target.Me, points: 0, buffs: new List<Buff>() {
                            new Buff(sourceCardId: 14, buffType: BuffTypes.AddDamage, duration: new Duration() { DurType = DurationTypes.EndingCondition, EndingCondition = EffectTypes.Damage, EndingConditionTarget = Target.Enemy }, points: 3, priority: 4, activationTime: ActivationTimeTypes.AtEnd, target: Target.Enemy, isOnTurnsView: false)
                        } ),
                } )
            },
            {15, new Card(id: 15, name: "Negate", discription: "", rarity: CardRarityes.Ultimate, type: CardTypes.C_Archmage, returnType: CardReturnToDeckTypes.Never,
                effects: new List<Effect>()
                {
                    new Effect(priority: 6, effectType: EffectTypes.DropToGrave, target: Target.Enemy, points: 0, buffs: null)
                } )
            },
            {16, new Card(id: 16, name: "Sacronica", discription: "", rarity: CardRarityes.Common, type: CardTypes.C_Necromancer, returnType: CardReturnToDeckTypes.AtNextRound,
                effects: new List<Effect>()
                {
                    new Effect(priority: 4, effectType: EffectTypes.Damage, target: Target.Select, points: 1, buffs: null),
                    new Effect(priority: 4, effectType: EffectTypes.Action, target: Target.Me, points: 0, buffs: new List<Buff>() {
                            new Buff(sourceCardId: 16, buffType: BuffTypes.ActivateClassAbilities, duration: new Duration() { DurType = DurationTypes.TurnsLeft, Turns = 1 }, priority: 1, isActive: false, isContinuous: true, canStack: false)
                        } ),
                } )
            },
            {17, new Card(id: 17, name: "Cadarkhas", discription: "", rarity: CardRarityes.Common, type: CardTypes.C_Necromancer, returnType: CardReturnToDeckTypes.AtNextRound,
                effects: new List<Effect>()
                {
                    new Effect(priority: 4, effectType: EffectTypes.Damage, target: Target.All, points: 2, buffs: null),
                    new Effect(priority: 4, effectType: EffectTypes.Action, target: Target.Enemy, points: 0, buffs: new List<Buff>() {
                            new Buff(sourceCardId: 17, buffType: BuffTypes.Damage, duration: new Duration() { DurType = DurationTypes.TurnsLeft, Turns = 2 }, points: 2, priority: 5, isActive: false, isContinuous: true, activationTime: ActivationTimeTypes.EveryTurn, target: Target.Me, canStack: false)
                        } ),
                } )
            },
            {18, new Card(id: 18, name: "Nacrayo", discription: "", rarity: CardRarityes.Ultimate, type: CardTypes.C_Necromancer, returnType: CardReturnToDeckTypes.Never,
                effects: new List<Effect>()
                {
                    new Effect(priority: 4, effectType: EffectTypes.Damage, target: Target.Select, points: 8, buffs: null)
                } )
            },
            {19, new Card(id: 19, name: "Blimpshot", discription: "", rarity: CardRarityes.Common, type: CardTypes.C_Blastmage, returnType: CardReturnToDeckTypes.AtNextRound,
                effects: new List<Effect>()
                {
                    new Effect(priority: 2, effectType: EffectTypes.Damage, target: Target.Select, points: 4, buffs: null)
                } )
            },
            {20, new Card(id: 20, name: "Enravel", discription: "", rarity: CardRarityes.Common, type: CardTypes.C_Blastmage, returnType: CardReturnToDeckTypes.AtNextRound,
                effects: new List<Effect>()
                {
                    new Effect(priority: 2, effectType: EffectTypes.MirrorDamage, target: Target.Me, points: 0, buffs:  new List<Buff>() {
                            new Buff(sourceCardId: 20, buffType: BuffTypes.MirrorDamage, duration: new Duration() { DurType = DurationTypes.TurnsLeft, Turns = 1 }, priority: 2)
                        } ),
                } )
            },
            {21, new Card(id: 21, name: "Ravite", discription: "", rarity: CardRarityes.Ultimate, type: CardTypes.C_Blastmage, returnType: CardReturnToDeckTypes.AtNextRound,
                effects: new List<Effect>()
                {
                    new Effect(priority: 2, effectType: EffectTypes.DamageUnblockable, target: Target.Enemy, points: 2, buffs: null)
                } )
            },
            {22, new Card(id: 22, name: "Gargo", discription: "", rarity: CardRarityes.Common, type: CardTypes.C_Vampire, returnType: CardReturnToDeckTypes.AtNextRound,
                effects: new List<Effect>()
                {
                    new Effect(priority: 4, effectType: EffectTypes.Action, target: Target.All, points: 0, buffs: new List<Buff>() {
                            new Buff(sourceCardId: 22, buffType: BuffTypes.HealToDamage, duration: new Duration() { DurType = DurationTypes.EndRound }, priority: 4, isContinuous: true)
                        } ),
                } )
            },
            {23, new Card(id: 23, name: "Kotu Rular", discription: "", rarity: CardRarityes.Common, type: CardTypes.C_Vampire, returnType: CardReturnToDeckTypes.AtNextRound,
                effects: new List<Effect>()
                {
                    new Effect(priority: 3, effectType: EffectTypes.Heal, target: Target.Select, points: 2, buffs: new List<Buff>() {
                            new Buff(sourceCardId: 23, buffType: BuffTypes.Heal, duration: new Duration() { DurType = DurationTypes.TurnsLeft, Turns = 1 }, points: 3, priority: 3, isActive: false, isContinuous: true, activationTime: ActivationTimeTypes.EveryTurn, target: Target.Me, canStack: false)
                        } ),
                } )
            },
            {24, new Card(id: 24, name: "Damnario", discription: "", rarity: CardRarityes.Ultimate, type: CardTypes.C_Vampire, returnType: CardReturnToDeckTypes.Never,
                effects: new List<Effect>()
                {
                    new Effect(priority: 6, effectType: EffectTypes.SwapHP, target: Target.All, points: 0, buffs: null)
                } )
            },
            {25, new Card(id: 25, name: "Emporum", discription: "", rarity: CardRarityes.Common, type: CardTypes.C_Demonic, returnType: CardReturnToDeckTypes.AtNextRound,
                effects: new List<Effect>()
                {
                    new Effect(priority: 4, effectType: EffectTypes.Damage, target: Target.Select, points: 2, buffs: new List<Buff>() {
                            new Buff(sourceCardId: 25, buffType: BuffTypes.Damage, duration: new Duration() { DurType = DurationTypes.TurnsLeft, Turns = 1 }, points: 3, priority: 5, isActive: false, isContinuous: true, activationTime: ActivationTimeTypes.EveryTurn, target: Target.Me ,canStack: false)
                        } ),
                } )
            },
            {26, new Card(id: 26, name: "Crypto", discription: "", rarity: CardRarityes.Common, type: CardTypes.C_Demonic, returnType: CardReturnToDeckTypes.AtNextRound,
                effects: new List<Effect>()
                {
                    new Effect(priority: 4, effectType: EffectTypes.Action, target: Target.Enemy, points: 0, buffs: new List<Buff>() {
                            new Buff(sourceCardId: 26, buffType: BuffTypes.DieAtBuffEnd, duration: new Duration() { DurType = DurationTypes.TurnsLeft, Turns = 3 }, priority: 6, isActive: false, isContinuous: true, activationTime: ActivationTimeTypes.AtEnd, target: Target.Me, canStack: false)
                        } ),
                } )
            },
            {27, new Card(id: 27, name: "Manaram", discription: "", rarity: CardRarityes.Ultimate, type: CardTypes.C_Demonic, returnType: CardReturnToDeckTypes.Never,
                effects: new List<Effect>()
                {
                    new Effect(priority: 6, effectType: EffectTypes.KillIfDamageDone, target: Target.Enemy, points: 30, buffs: null)
                } )
            },
        };


        //разбивка всех карт на колоды по классам(персонажам)
        CardsDicByCharId = new Dictionary<int, List<Card>>();
        for (int i = 0; i < CharsDic.Count + 1; i++) //кол-во персонажей смотрю + Ancient карты, ктр будут под индексом 0 в словаре.        
            CardsDicByCharId.Add(i, CardsDic.Values.Where(x => x.Type == (CardTypes)i).ToList());        

        BuffsImgsDic = new Dictionary<int, Sprite>();
        foreach (ImagesKeyPair kvp in BuffsImgs)
            BuffsImgsDic.Add(kvp.Key, kvp.Img);

        BuffsClassAbilitiesImgsDic = new Dictionary<int, Sprite>();
        foreach (ImagesKeyPair kvp in BuffsClassAbilitiesImgs)
            BuffsClassAbilitiesImgsDic.Add(kvp.Key, kvp.Img);
    }



    public Sprite[] CharactersPortraitImages;
    public static Dictionary<int, Character> CharsDic;


    public Dictionary<AnimTypes, AnimationClip> AnimDic;
    public AnimationClip[] Anims;

    public static Dictionary<int, Card> CardsDic;
    public Sprite[] CardsImgs;
    public Sprite CardPlaceImg;

    public static Dictionary<int, List<Card>> CardsDicByCharId;
    public Sprite[] TokenImgs;
        
    [Serializable]
    public class ImagesKeyPair
    {
        public int Key;
        public Sprite Img;
    }    
    public List<ImagesKeyPair> BuffsImgs;
    public Dictionary<int, Sprite> BuffsImgsDic;
    public List<ImagesKeyPair> BuffsClassAbilitiesImgs;
    public Dictionary<int, Sprite> BuffsClassAbilitiesImgsDic;

    public Sprite[] EndGameImages;



    public static readonly Dictionary<BuffTypes, string> BuffNameDic = new Dictionary<BuffTypes, string>()
    {        
        { BuffTypes.NoDamage, "Иммунитет к урону" },
        { BuffTypes.NoBuffs, "Иммунитет к продолжительным заклинаниям" },
        { BuffTypes.CantDie, "Иммунитет к смерти" },
        { BuffTypes.AddDamage, "Баф к следущей атаке" }, //сколько к дамагу прибавить
        { BuffTypes.ActivateClassAbilities, "Активация способностей" },
        { BuffTypes.HealToDamage, "Урон вместо восстановления" },
        { BuffTypes.Heal, "Восстановление здоровья" }, //сколько лечения
        { BuffTypes.Damage, "Получение урона" }, //сколько урона
        { BuffTypes.DieAtBuffEnd, "Смертельное проклятие" },
        { BuffTypes.MirrorActions, "Отражение заклинаний" },
        { BuffTypes.MirrorDamage, "Отражение урона" },

        { BuffTypes.MultiplyCommonDamage, "Умножение урона" },
        { BuffTypes.IncDamage, "Прибавление к урону" },
        { BuffTypes.ShowDamageDealt, "Manaram" },
    };    

    public static readonly Dictionary<DurationTypes, string> DurationNameDic = new Dictionary<DurationTypes, string>()
    {        
        { DurationTypes.EndRound, "до конца раунда"},        
        { DurationTypes.TurnsLeft, "до прошествия ходов - "},
        { DurationTypes.EndingCondition, "до события - "},
        { DurationTypes.EndGame, "до конца игры"},
    };    

    public static readonly Dictionary<EffectTypes, string> EffectNameDic = new Dictionary<EffectTypes, string>()
    {
        { EffectTypes.Action, "Заклинание"},
        { EffectTypes.CancelActions, "Отмена действий карт"},
        { EffectTypes.CancelBuffs, "Отмена бафов"},
        { EffectTypes.Heal, "Восстановление"},
        { EffectTypes.MirrorActions, "Отражение"},
        { EffectTypes.Damage, "Урон"},
        { EffectTypes.CancelAction, "Отмена действий оппонента с приоритетом "},
        { EffectTypes.DropToGrave, "Уничтожение карт в сбросе оппонента"},
        { EffectTypes.MirrorDamage, "Отражение урона"},
        { EffectTypes.DamageUnblockable, "Игнорирование иммунитета к урону"},
        { EffectTypes.SwapHP, "Обмен здровьем"},
        { EffectTypes.KillIfDamageDone, "Убить оппонента"},
        { EffectTypes.Kill, "Смерть от проклятия"},
        { EffectTypes.Empty, "Нет эффекта"},
    };

    public static readonly int[][] CardsYPosArray = new int[][] 
    {
        new int[] { 0 },
        new int[] { 0 }, //1
        new int[] { 0,0 }, //2
        new int[] { -10, 0, -10 }, //3
        new int[] { -10, 0, 0, -10 }, //4
        new int[] { -15, 0, 5, 0, -15 }, //5
        new int[] { -20, -5, 5, 5, -5, -20 }, //6
        new int[] { -20, -5, 5, 8, 5, -5, -20 }, //7
        new int[] { -20, -5, 5, 10, 10, 5, -5, -20 }, //8
        new int[] { -25, -10, 0, 5, 8, 5, 0, -10, -25 }, //9
        new int[] { -25, -10, 0, 5, 8, 8, 5, 0, -10, -25 }, //10
        new int[] { -25, -10, 0, 5, 8, 10, 8, 5, 0, -10, -25 }, //11
        new int[] { -25, -10, 0, 5, 8, 10, 10, 8, 5, 0, -10, -25 }, //12
        new int[] { -25, -10, 0, 5, 8, 10, 12, 10, 8, 5, 0, -10, -25 }, //13
        new int[] { -25, -10, 0, 5, 8, 10, 12, 12, 10, 8, 5, 0, -10, -25 }, //14
        new int[] { -25, -10, 0, 5, 8, 10, 12, 12, 12, 10, 8, 5, 0, -10, -25 }, //15
        new int[] { -25, -10, 0, 5, 8, 10, 12, 12, 12, 12, 10, 8, 5, 0, -10, -25 }, //16
        new int[] { -25, -10, 0, 5, 8, 10, 12, 12, 12, 12, 12, 10, 8, 5, 0, -10, -25 }, //17
        new int[] { -25, -10, 0, 5, 8, 10, 12, 12, 12, 12, 12, 12, 10, 8, 5, 0, -10, -25 }, //18
        new int[] { -25, -10, 0, 5, 8, 10, 12, 12, 12, 12, 12, 12, 12, 10, 8, 5, 0, -10, -25 }, //19
        new int[] { -25, -10, 0, 5, 8, 10, 12, 12, 12, 12, 12, 12, 12, 12, 10, 8, 5, 0, -10, -25 }, //20
        new int[] { -25, -10, 0, 5, 8, 10, 12, 12, 12, 12, 12, 12, 12, 12, 12, 10, 8, 5, 0, -10, -25 }, //21
        new int[] { -25, -10, 0, 5, 8, 10, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 10, 8, 5, 0, -10, -25 }, //22
        new int[] { -25, -10, 0, 5, 8, 10, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 10, 8, 5, 0, -10, -25 }, //23
        new int[] { -25, -10, 0, 5, 8, 10, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 10, 8, 5, 0, -10, -25 }, //24
        new int[] { -25, -10, 0, 5, 8, 10, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 10, 8, 5, 0, -10, -25 }, //25
    };

    public static readonly Vector2[][] BuffsPosArrayOnTurns = new Vector2[][]
    {
        new Vector2[] { new Vector2(0,0) },
        new Vector2[] { new Vector2(0f, 0f) }, //1
        new Vector2[] { new Vector2(-20f, 0f),  new Vector2(20f, 0f) }, //2
        new Vector2[] { new Vector2(-25, 0f), new Vector2(0f, 0f), new Vector2(25f, 0f) }, //3
        new Vector2[] { new Vector2(-25, 0f), new Vector2(0f, 0f), new Vector2(25f, 0f), new Vector2(-12.5f, 15f) }, //4
        new Vector2[] { new Vector2(-25, 0f), new Vector2(0f, 0f), new Vector2(25f, 0f), new Vector2(-12.5f, 15f), new Vector2(12.5f, 15f) }, //5
    };

    public static readonly Vector2[][] BuffsPosArrayOnPortrait = new Vector2[][]
    {
        new Vector2[] { new Vector2(0,0) },
        new Vector2[] { new Vector2(-14.2f, 42.2f) }, //1
        new Vector2[] { new Vector2(-14.2f, 42.2f),  new Vector2(-55.2f, 5.6f) }, //2
        new Vector2[] { new Vector2(-14.2f, 42.2f), new Vector2(-43.11f, 12.3f), new Vector2(-81.6f, -15.5f) }, //3
        new Vector2[] { new Vector2(-14.2f, 52.8f), new Vector2(-36.7f, 25.9f), new Vector2(-62.6f, -0.3f), new Vector2(-88.4f, -23.7f) }, //4
        new Vector2[] { new Vector2(-14.2f, 52.8f), new Vector2(-30.7f, 31.72f), new Vector2(-48.18f, 12.14f), new Vector2(-68.9f, -7.3f), new Vector2(-88.4f, -23.7f) }, //5
    };

}
