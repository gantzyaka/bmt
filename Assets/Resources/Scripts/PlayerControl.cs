﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using GameTypes;

public class PlayerControl : NetworkBehaviour
{
    private void Start()
    {
        if (isLocalPlayer)        
            GameManager.instance.PlayerControlMy = this;        
        else        
            GameManager.instance.PlayerControlEnemy = this;        
    }


    void Update()
    {
        //тестить команды
        if (isLocalPlayer)
        {
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                string shit0 = "";
                string shit1 = "";
                GameManager.GetSomeShit(ref shit0, ref shit1);
                MenuManager.instance.DebugText0.text = shit0;                
                MenuManager.instance.DebugText1.text = shit1;
                MenuManager.instance.Debug.SetActive(true);
            }
            else if (Input.GetKeyDown(KeyCode.DownArrow))            
                MenuManager.instance.Debug.SetActive(false);            
        }
    }

    public override void OnStartLocalPlayer()
    {
        base.OnStartLocalPlayer();

        if (!GameManager.isHost)
            CmdSendNameToServer(GameManager.MyCharacterId, GameManager.MyName, GameManager.MyId);
    }


    [Command]
    public void CmdSendNameToServer(int charId, string name, int id)
    {
        RpcSetPlayerName(charId, name, id);
    }

    [ClientRpc]
    void RpcSetPlayerName(int charId, string name, int id)
    {
        GameManager.SetPlayerParams(charId, name, id);
        if (GameManager.MyId != id)
        {
            MenuManager.instance.PreGameMenuShow();
            GameManager.State = GameStates.SelectCards;

            //если хост получил даные опонента, то отправить свои данные в ответ
            if (GameManager.isHost)            
                CmdSendNameToServer(GameManager.MyCharacterId, GameManager.MyName, GameManager.MyId);            
        }        
    }

    //моё действие в игре
    public void MyAction(Actions act, string mes)
    {
        if (isLocalPlayer) //только мой объект игрока обрабатывает мои действия
        {
            //хост отправляет свои дейтсвия сразу в логику игры; клиент - отправляет хосту, а хост уже отпарвляет их в логику игры, которая считается у хоста.
            if (GameManager.isHost)
            {
                Debug.Log("MyAction, isLocalPlayer, isHost");                
                GameManager.UpdateGame(GameManager.MyId, act, mes);
            }
            else
            {
                Debug.Log("MyAction, isLocalPlayer, !isHost");
                CmdMyActionSend(act, mes);
            }
        }
        else        
            Debug.Log("MyAction, !isLocalPlayer");        
    }
    

    #region Команды от клиента хосту
    [Command]
    public void CmdMyActionSend(Actions act, string mes)
    {        
        Debug.Log("CmdMyActionSend: act = " + act.ToString() + " ; mes = " + mes);
        RpcRecievedAction(act, mes);
    }

    [ClientRpc]
    void RpcRecievedAction(Actions act, string mes)
    {        
        //если я - хост, получил команду от клиента, отправляю её в логику игры
        if (GameManager.isHost && !isLocalPlayer)
        {
            Debug.Log("RpcRecievedAction: act = " + act + " ; mes = " + mes);            
            GameManager.UpdateGame(GameManager.EnemyId, act, mes);
        }
    }
    #endregion
    

    #region Команды от хоста клиенту
    [Command]
    public void CmdResultSend(int id, Actions act, string mes)
    {
        Debug.Log("CmdResultSend: id =  " + id.ToString() + "act = " + act.ToString() + " ; mes = " + mes);
        RpcRecievedResult(id, act, mes);
    }

    [ClientRpc]
    void RpcRecievedResult(int id, Actions act, string mes)
    {
        //если я - клиент, получил команду от хоста, обновляю у себя состояние игры по полученному результату
        if (!GameManager.isHost && !isLocalPlayer)
        {
            Debug.Log("RpcRecievedResult: id = " + id.ToString() + " ; act = " + act.ToString() + " ; mes = " + mes);
            GameManager.UpdateGame(id, act, mes);
        }
    }
    #endregion
}