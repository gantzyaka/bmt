﻿using System.Collections.Generic;
using System.Threading;
using System;
using System.Linq;

namespace GameTypes
{
    public class Player
    {
        public string Name { get; private set; } = "NoName";
        public int CharId { get; private set; } = 1;
        public bool IsHost { get; private set; } = false;
        public int ConnectionId { get; private set; } = -1;
        public bool IsEnemy { get; private set; }
        public int Id { get; private set; }
        public int HP { get; private set; }
        public int HPMax { get; private set; } // у всех, кроме вампира
        public int DamageDealt { get; private set; }  //сколько урона нанес врагу за игру 
        public int SumHealAtTurn { get; private set; }
        public int SumDamageAtTurn { get; private set; }
        private Dictionary<int, int> DamageGotAtTurnByPriority = new Dictionary<int, int>();        

        public List<Card> DeckPattern { get; private set; } = new List<Card>(); //колода-образец всех карт, ктр доступны игроку
        public List<Card> Deck { get; private set; } = new List<Card>(); //колода игрока
        public List<Card> Hand { get; private set; } = new List<Card>(); //рука
        public List<Card> Drop { get; private set; } = new List<Card>(); //сброс
        public List<Card> Grave { get; private set; } = new List<Card>(); //могила
        public bool IsReady { get; private set; } //когда нажал кнопку Ready в игре        
        public int RoundWithUsedFreeToken { get; private set; } //раунд, в котором игрок использовал бесплатное копирование карты, обнуляется на второй раунд с момента использования
        public int Tokens { get; private set; } = 2; //количество копирований карт игрока, доступных после использования freeToken, не восполняются до конца игры        
        public List<Buff> Buffs { get; private set; } = new List<Buff>(); //продолжительные бафы (приоретаются с разыгранных карт, время действия у них внутри)
        public ReactCommit LastReact { get; private set; } //инфа о последней замене карты игроком в реакт-стадии
        public int GatherCards { get; private set; } = -1; //карт может добрать в руку из колоды, каждый раунд дается 3, пока не возьмет 3 карты - не давать нажимать кнопку "готов"

        //не синхронизировать с хостом
        public int RoundSee { get; private set; } = 0; //открытый раунд, ктр игрок сейчас смотрит на столе
        public int SelectedCard { get; private set; } = -1;
        public int SelectedPlace { get; private set; } = -1;
        public int SelectedTarget { get; private set; } = -1;
        public bool DeskSee { get; private set; } = true; //смотрит игровой стол или колоду добора карт        
        public List<int> SelectedDeckCards { get; private set; } = new List<int>();
        private CardsEqualityComparer CardsComparer = new CardsEqualityComparer();
        public event DelBool OnGatherFull; //событие, когда игрок выбрал нужное количество карт (3) для добора из колоды
        public event DelDeckIcoText OnDecksChange; //событие, когда у игрока изменилось количество карт в колоде/сбросе/могиле
        public event DelReactIco OnReactChange; //событие, когда у игрока изменилось право менять карту в рекат-стадии
        public event DelCopiesIco OnCopiesChange; //событие, когда у игрока изменилось количество доступных копирований карт
        public event DelBuffsView OnBuffsChange; //событие, когда у игрока изменились бафы
        public bool IsEventsSubscribed { get; private set; }


        public void UpdateBuffsView()
        {
            OnBuffsChange?.Invoke(IsEnemy, Buffs);
        }        

        public void SubscribeOnEvents(DelDeckIcoText delDeck, DelReactIco delReact, DelCopiesIco delCopies, DelBuffsView delOnPortraitBuffs, DelBuffsView delOnTurnsBuffs)
        {
            if (delDeck == null || delReact == null || delCopies == null || delOnPortraitBuffs == null || delOnTurnsBuffs == null) return;

            if (!IsEventsSubscribed)
            {
                OnDecksChange += delDeck;
                OnReactChange += delReact;
                OnCopiesChange += delCopies;

                OnBuffsChange += delOnPortraitBuffs;
                OnBuffsChange += delOnTurnsBuffs;

                IsEventsSubscribed = true;
            }

            UpdateDecksIcoTexts(isReactStage: false);
            CopiesInvoke();
            UpdateBuffsView();            
        }

        public void UnsubscribeOnEvents()
        {
            if (!IsEventsSubscribed) return;

            if (OnDecksChange != null)
                foreach (DelDeckIcoText del in OnDecksChange.GetInvocationList())
                    OnDecksChange -= del;

            if (OnReactChange != null)
                foreach (DelReactIco del in OnReactChange.GetInvocationList())
                    OnReactChange -= del;

            if (OnCopiesChange != null)
                foreach (DelCopiesIco del in OnCopiesChange.GetInvocationList())
                    OnCopiesChange -= del;

            if (OnBuffsChange != null)
                foreach (DelBuffsView del in OnBuffsChange.GetInvocationList())
                    OnBuffsChange -= del;

            IsEventsSubscribed = false;
        }

        public void CopiesInvoke(int closedCopies = 0)
        {
            OnCopiesChange?.Invoke(IsEnemy, Tokens + (RoundWithUsedFreeToken == -1 ? 1 : 0) + closedCopies); //обновить отображаемое количество доступных копий
        }

        public bool HaveEnoughCardsForRound
        {
            get { return Hand.Count + Deck.Count >= 3; }
        }

        public void UpdateDecksIcoTexts(bool isReactStage, int virtualHandDec = 0)
        {
            if (IsEnemy && isReactStage) return; //в реакт стадии не показыватьк как меняются числа у противника

            int hand = Hand.Count;

            if (IsEnemy)
                hand -= virtualHandDec;

            OnDecksChange?.Invoke(IsEnemy, hand, Deck.Count, Drop.Count, Grave.Count);
        }

        private void AddDamageGotAtTurnByPriority(int priority, int damage)
        {
            if (damage <= 0 || priority < 0 || priority > 9) return;

            if (DamageGotAtTurnByPriority.ContainsKey(priority))
                DamageGotAtTurnByPriority[priority] += damage;
            else
                DamageGotAtTurnByPriority.Add(priority, damage);
        }

        public bool DropToGrave(ref string text)
        {
            if (Drop.Count == 0) return false;

            foreach (Card c in Drop)
                text += Lib.CardsDic[c.Id].Name + ", ";

            text = text.Remove(text.Length - 2);

            Grave.AddRange(Drop);
            Drop.Clear();

            return true;
        }

        public void DevidePlayedCards(List<CardWithWay> tuples)
        {
            if (tuples == null || tuples.Count < 1) return;

            foreach (CardWithWay cb in tuples)
            {
                if (cb.Card == null) continue;

                if (cb.Way != WayForCard.None) //в первую очередь карта улетает по направлению, заданному в месте, где лежит (Sinx - вернуть в колоду, Archmage - на всегда в могилу)
                {
                    if (cb.Way == WayForCard.ToGrave)
                        DevideCardTo(cb.Card, Grave);
                    else if (cb.Way == WayForCard.ToDeck)
                        DevideCardTo(cb.Card, Deck);
                }
                else //в последнюю очередь карта улетает по направлению, заданному в ней самой
                {
                    if (cb.Card.ReturnType == CardReturnToDeckTypes.Never)  //Ultimate карты в основном - улетают сразу в могилу, не возвращаются в колоду
                        DevideCardTo(cb.Card, Grave);
                    else if (cb.Card.ReturnType == CardReturnToDeckTypes.AtNextRound) //Common карты в основном - улетает в сброс, возвращаются в колоду под конец следущего раунда
                        DevideCardTo(cb.Card, Drop);
                }
            }
        }

        private void DevideCardTo(Card c, List<Card> ToList)
        {
            if (c == null || ToList == null) return;

            if (!ToList.Contains(c, CardsComparer))
                ToList.Add(c);
        }

        public void ReturnCardsFromDrop()
        {
            if (Drop.Count > 0)
            {
                Deck.AddRange(Drop);
                Drop.Clear();
            }
        }

        public List<Card> GatherCardsFromDeckToHand(string[] ss)
        {
            if (ss == null || ss.Length == 0 || GatherCards == -1) return null;

            List<Card> cards = new List<Card>();

            foreach (int i in Array.ConvertAll(ss, int.Parse))
            {
                if (i >= Deck.Count) continue;
                cards.Add(Deck[i]);
            }

            foreach (Card c in cards)
            {
                Hand.Add(new Card(c.Id));
                Deck.Remove(c);
            }

            GatherCards = -1;

            if (!IsEnemy)
            {
                SelectedDeckCards.Clear();
                OnGatherFull?.Invoke(false);
            }

            return cards;
        }

        public bool AddSelectedDeckCard(int i)
        {
            if (GatherCards <= 0 || i >= Deck.Count || SelectedDeckCards.Count >= 3 || SelectedDeckCards.Contains(i))
                return false;

            SelectedDeckCards.Add(i);
            GatherCards--;

            if (GatherCards == 0)
                OnGatherFull?.Invoke(true);

            return true;
        }

        public bool RemoveSelectedDeckCard(int i)
        {
            if (GatherCards < 0 || GatherCards >= 3 || SelectedDeckCards.Count == 0)
                return false;

            if (SelectedDeckCards.Remove(i))
            {
                GatherCards++;

                if (GatherCards > 0)
                    OnGatherFull?.Invoke(false);

                return true;
            }
            else
                return false;
        }

        public void CreateHand(string[] ss)
        {
            foreach (string s in ss)
                Hand.Add(new Card(Int32.Parse(s)));
        }

        public void CreateDeckFromDeckPatternExceptHand()
        {
            Deck = DeckPattern.Except(Hand, CardsComparer).ToList();
        }

        public void DeskSeeChange()
        {
            DeskSee = !DeskSee;
        }

        public void SetNewHeal(int heal, int sumDamageAtTurn, int sumHealAtTurn) //используется в эффекте EffectTypes.SwapHP и EffectTypes.Kill
        {
            HP = heal;
            SumDamageAtTurn = sumDamageAtTurn;
            SumHealAtTurn = sumHealAtTurn;

            if (HP > HPMax && HPMax > 0)
                HP = HPMax;
        }

        public List<PlayedCardEffect> GetPlayedCardEffectsFromBuffs(int i, List<PlayedCardEffect> alreadyAddedEffects)
        {
            List<PlayedCardEffect> pces = new List<PlayedCardEffect>();

            foreach (Buff b in Buffs.ToList())
            {
                bool ok = false;

                if (b.ActivationTime == ActivationTimeTypes.AtEnd) //бафы, которые срабатывают при наступлении события, и сгорают
                {
                    if (b.Duration.DurType == DurationTypes.EndingCondition) //если событие - разыгранная этим же игроком карта с заданным эффектом в заданном направлении
                    {
                        if (alreadyAddedEffects.Any(x => x.PlayerId == i && x.Effect.EffectType == b.Duration.EndingCondition && b.EqualsOrIncludedInEndingConditionTarget(x.Effect.Target != Target.Select ? x.Effect.Target : x.Target)))
                        {
                            if (b.CheckForDie(dur: DurationTypes.EndingCondition, conditionIsDone: true))
                            {
                                Buffs.Remove(b);
                                ok = true;
                            }
                        }
                    }
                    else if (b.Duration.DurType == DurationTypes.TurnsLeft && b.Duration.Turns == 1 && b.IsActive) //если событие - окончание заданного количества ходов висения на игроке бафа
                    {
                        if (b.CheckForDie(dur: DurationTypes.TurnsLeft))
                        {
                            ok = true;
                            Buffs.Remove(b);
                        }
                    }
                }
                //бафы, которые срабатывают каждый ход, пока висят на игроке
                else if (b.ActivationTime == ActivationTimeTypes.EveryTurn)
                {
                    ok = true;
                }
                //p.s. а бафы, которые работают всегда, - у них нет никаких действий, которые нужно отдельно запускать.

                if (ok)
                {
                    pces.Add(new PlayedCardEffect(playerId: i, target: b.Target, cardId: b.SourceCardId, effect: new Effect(b), place: null, charId: b.SourceCharId, name: b.SourceCardId > 0 ? Lib.CardsDic[b.SourceCardId].Name : "эффект"));
                }
            }

            if (pces.Count > 0)
                UpdateBuffsView();

            return pces;
        }

        public void CancelAllBuffs()
        {
            if (Buffs == null || Buffs.Count == 0) return;

            Buffs.RemoveAll(x => !x.IsClassAbility);            
            UpdateBuffsView();                        
        }

        public void ChangeBuffsByApplingDuration(DurationTypes dur)
        {
            foreach (Buff b in Buffs.ToList())
                if (b.CheckForDie(dur))
                    Buffs.Remove(b);            
        }

        public void ResetByRound(int round)
        {
            if (round <= 0) return;

            //возможность замены карты в реакт-стадии у игроков сбрасывается на второй раунд с последней замены
            if (LastReact != null && round - LastReact.AtRound >= 2)
                ReactCommit(null);

            //бесплатный токен сбрасывается на второй раунд с его использования
            if (RoundWithUsedFreeToken > -1 && round - RoundWithUsedFreeToken >= 2)            
                RoundWithUsedFreeToken = -1;

            OnCopiesChange?.Invoke(IsEnemy, Tokens + (RoundWithUsedFreeToken == -1 ? 1 : 0)); //в конце раунда у всех обновить отображаемое количество доступных копий

            if (Deck.Count > 0)
                GatherCards = Deck.Count >= 3 ? 3 : Deck.Count; //3 карты может добрать в руку из колоды (или сколько у него там) //если нет ничего в колоде - не надо добирать

            if (!IsEnemy)
                DeskSee = false;
        }

        public void ReactCommit(ReactCommit r)
        {
            LastReact = r;

            if (!IsEnemy) //у противника не показываем, она отдельно обновляется в конце раунда
                OnReactChange?.Invoke(IsEnemy, LastReact == null);
        }

        public int AddHeal(int heal)
        {
            if (HP == 0) return 0;

            int healResult = heal;

            HP += heal;

            if (HP > HPMax && HPMax > 0)
                HP = HPMax;

            SumHealAtTurn += heal;

            return healResult;
        }

        public void AddDamageDealt(int damage)
        {
            DamageDealt += damage;

            if (this.CharId == 4)
            { 
                Buffs.First(x => x.BuffType == BuffTypes.ShowDamageDealt).SetPoints(DamageDealt);
                UpdateBuffsView();
            }
        }

        public int AddDamage(int damage, bool unblockable, int priority)
        {
            int damResult = damage;

            if (!unblockable && CheckForBuff(BuffTypes.NoDamage))
                damResult = 0;

            if (CharId == 4 && damResult > 0) //Demonic - Не может получить больше 3 ед. урона в течение одного приоритета
            {
                if (DamageGotAtTurnByPriority.ContainsKey(priority))
                {
                    int oldDamage = DamageGotAtTurnByPriority[priority];

                    if (oldDamage >= 3)
                        damResult = 0;
                    else if ((damResult + oldDamage) > 3)
                        damResult = 3 - oldDamage;
                }
                else if (damResult > 3)
                    damResult = 3;
            }

            if (damResult > 0)
            {
                int residual = damResult - HP;
                HP -= damResult;

                if (HP < 0) HP = 0;

                if (HP == 0 && CheckForBuff(BuffTypes.CantDie))
                {
                    HP = 1;
                    damResult = damResult - residual - 1;
                }

                SumDamageAtTurn += damResult;
                AddDamageGotAtTurnByPriority(priority, damResult);
            }

            return damResult;
        }        

        public bool AddBuff(Buff b, int sourcePlayerId, int sourceCharId, Target tg, bool updateView = true)
        {
            if (Buffs.Any(x => x.BuffType == BuffTypes.NoBuffs) && b.IsContinuous && !b.IsClassAbility) //баф, запрещающий навешивать продолжительные бафы
                return false;

            //проверка на дублирования, сложение бафов на дамагу
            Buff finded = Buffs.FirstOrDefault(x => x.BuffType == b.BuffType && x.SourceCardId == b.SourceCardId);

            if (finded != null && b.CanStack) //стакающиеся бафы складываются в один
                finded.AddPoints(b.Points);
            else
                Buffs.Add(new Buff(b, sourcePlayerId, sourceCharId, tg));

            if (updateView)
                UpdateBuffsView();

            return true;
        }

        public bool CheckForBuff(BuffTypes type)
        {
            return Buffs.Any(x => x.BuffType == type && x.IsActive);
        }

        public void ResetTokens()
        {
            RoundWithUsedFreeToken = -1;
            Tokens = DefaultTokens;
        }

        public void ResetSelecting()
        {
            SelectedCard = -1;
            SelectedPlace = -1;
            SelectedTarget = -1;
        }

        public void Reset()
        {
            ResetSelecting();
            ResetAllDecks();
            RoundSee = 0;
            DamageDealt = 0;
            IsReady = false;
            ResetTokens();
            Buffs.Clear();
            ResetAtTurn();
            LastReact = null;
            DeskSee = true;
            GatherCards = -1;
            SelectedDeckCards.Clear();

            HP = Lib.CharsDic[CharId].HP;
        }

        public void ResetAllDecks()
        {
            DeckPattern.Clear();
            Deck.Clear();
            Hand.Clear();
            Drop.Clear();
            Grave.Clear();
        }

        public void ResetAtTurn()
        {
            SumDamageAtTurn = 0;
            SumHealAtTurn = 0;

            DamageGotAtTurnByPriority.Clear();
        }

        public bool HasTokens
        {
            get { return Tokens > 0 || RoundWithUsedFreeToken == -1; }
        }

        private int DefaultTokens
        {
            get { return CharId != 2 ? 2 : 4; } //у Blastmage по умолчанию 4 токена вместо 2
        }

        public void UseTokens(bool decrease, int round)
        {
            if (decrease)
            {
                if (RoundWithUsedFreeToken == -1)
                    RoundWithUsedFreeToken = round;
                else if (Tokens > 0)
                    Tokens--;
            }
            else
            {
                if (Tokens < DefaultTokens)
                    Tokens++;
                else if (RoundWithUsedFreeToken > -1)
                    RoundWithUsedFreeToken = -1;
            }

            if (!IsEnemy) //у противника не показываем, у него отдельно обновляется в конце раунда
                OnCopiesChange?.Invoke(IsEnemy, Tokens + (RoundWithUsedFreeToken == -1 ? 1 : 0));
        }

        public void SetSelectedCard(int i)
        {
            SelectedCard = i;
        }

        public void SetName(string s)
        {
            Name = s;
        }

        public void SetChar(int i)
        {
            CharId = i;
            Character ch = Lib.CharsDic[i];
            HP = ch.HP;
            HPMax = ch.HasLimitHP ? ch.HP : 0;

            ResetDamageDealt();
            ResetAllDecks();
            ResetTokens();
            Buffs.Clear();

            DeckPattern.AddRange(Lib.CardsDicByCharId[i]); //3 классовые карты
            DeckPattern.AddRange(Lib.CardsDicByCharId[0]); //12 общих карт    

            if (ch.Id == 4) //на демонике в с начала и до конца игры должен висеть бафф Manaram
            {
                Buff b = new Buff(sourceCardId: -1, buffType: BuffTypes.ShowDamageDealt, duration: new Duration() { DurType = DurationTypes.EndGame }, points: DamageDealt, canStack: false, isContinuous: true, isOnTurnsView: false, isClassAbility: true);
                this.AddBuff(b, this.Id, this.CharId, Target.None, updateView: false);
            }
        }

        public void SetHost(bool ok)
        {
            IsHost = ok;
        }

        public void SetConnectionId(int i)
        {
            ConnectionId = i;
        }

        public void SetEnemy(bool ok)
        {
            IsEnemy = ok;
        }

        public void SetArrayId(int id)
        {
            if (id < 0 || id > 1) return;

            Id = id;
        }

        public int IdOfPlayerPlace
        {
            get { return IsEnemy ? 1 : 0; }
        }

        public void SetIsReady(bool ok)
        {
            IsReady = ok;

            if (LastReact != null && !LastReact.IsDone)
                LastReact.SetIsDone();
        }

        public void ResetDamageDealt()
        {
            DamageDealt = 0;
        }

        public void SetRoundSee(int round)
        {
            RoundSee = round;
        }
    }
}
