﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Net;
using System.Net.Sockets;
using GameTypes;

public class CustomNetworkManager : NetworkManager {

    public List<PlayerControl> Players = new List<PlayerControl>();
    
    public static CustomNetworkManager instance { get; private set; }

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
    }        

    //когда новый игрок присоединился (только у хоста вызывается)
    public override void OnServerAddPlayer(NetworkConnection nc, short playerControllerId)
    {
        int count = NetworkServer.connections.Count;
        if (count <= startPositions.Count) //если ему нет точки спавна (всего 2 точки)
        {
            GameObject player = Instantiate(playerPrefab, startPositions[count - 1].position, Quaternion.identity);
            NetworkServer.AddPlayerForConnection(nc, player, playerControllerId);
            GameManager.SetConnectionId(count - 1, nc.connectionId); //запоминаем сетевой айди в игроке //это только для хоста нужно            
            Players.Add(player.GetComponent<PlayerControl>());             
            Debug.Log("OnServerAddPlayer. player " + (count-1) + " conId = " + nc.connectionId);

            if (count == 2) //когда 2 игрока - создаем матч            
                MenuManager.instance.PreGameMenuWait();    
        }
        else
        {
            nc.Disconnect(); //разорвать с ним соединение
            Debug.Log("Разрываю соединение!");
        }
    }

    public override void OnClientDisconnect(NetworkConnection nc)
    {        
        base.OnClientDisconnect(nc);        
        StopClient();
        MenuManager.instance.GoToMainMenu();
        Debug.Log("OnClientDisconnect");
        GameManager.ResetGame();
        MenuManager.instance.ResetAllOnEnd();
        SoundManager.instance.CardClickSound();
    }

    public override void OnStopHost()
    {
        base.OnStopHost();
        Debug.Log("OnStopHost");
        StopClient();
        GameManager.ResetGame();
        MenuManager.instance.ResetAllOnEnd();
    }

    public override void OnServerDisconnect(NetworkConnection nc)
    {
        Debug.Log("OnServerDisconnect: nc.connectionId = " + nc.connectionId + " ; GameManager.EnemyConnectionId = " + GameManager.EnemyConnectionId);
        if (nc.connectionId == GameManager.EnemyConnectionId)
        {
            base.OnServerDisconnect(nc);
            if (GameManager.isHost) CustomNetworkManager.instance.StopHost();
            NetworkServer.DestroyPlayersForConnection(nc);            
            MenuManager.instance.GoToMainMenu();
            GameManager.ResetGame();        
            MenuManager.instance.ResetAllOnEnd();

            Debug.Log("Отключился: " + GameManager.EnemyName);
            SoundManager.instance.CardClickSound();
        }        
    }
        
        
    

    /////////////////////////////    
    public void StartupHost()
    {
        GameManager.MyId = 0; //я хост, первый в массиве игроков        
        GameManager.ResetPlayer(1); //сбросить врага старого
        SetPort();
        instance.StartHost();
        GameManager.SetMeHost();
        MenuManager.instance.SwitchMenu();        
    }

    public void JoinGame()
    {
        GameManager.MyId = 1; //я клиент, второй в массиве игроков
        GameManager.ResetPlayer(0); //сбросить врага старого
        SetIPAddress();
        SetPort();
        instance.StartClient();
        MenuManager.instance.SwitchMenu();        
    }

    void SetIPAddress()
    {
        string ipAddress = MenuManager.instance.InputFieldIP.text;
        instance.networkAddress = ipAddress;
    }

    void SetPort()
    {
        instance.networkPort = 7777;
    }

    public void Disconnect()
    {        
        instance.StopHost();
        MenuManager.instance.GoToMainMenu();
        GameManager.ResetMeHost();
    }

    public override void OnServerConnect(NetworkConnection nc)
    {
        if (NetworkServer.connections.Count < 2)        
            base.OnServerConnect(nc);
    }

    public override void OnClientConnect(NetworkConnection nc)
    {
        base.OnClientConnect(nc);        
    }

    public static string GetMyIPAddress()
    {
        try
        {
            return new WebClient().DownloadString("http://icanhazip.com");
        }
        catch
        {
            return "icanhazip.com недоступен, посмотрите свой внешний ip еще где-нибудь, например на 2ip.ru";
        }
    }

    
}
