﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GameTypes;

public class ToolTipCardControl : MonoBehaviour {

    public int Id;
    public Image Img;
    public Button Btn;
    
    public RectTransform RTrans;        

    private void Awake()
    {        
        RTrans = GetComponent<RectTransform>();        
    }

    private void Start()
    {
        Img.sprite = Lib.instance.CardsImgs[0];
    }

    public void UpdateToolTip(int cid, bool show)
    {
        if (cid == -1) return;

        Img.sprite = show ? Lib.instance.CardsImgs[cid] : Lib.instance.CardsImgs[0];
    }    
}
